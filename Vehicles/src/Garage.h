#ifndef VEHICLES_GARAGE_H
#define VEHICLES_GARAGE_H

#include <vector>
#include <algorithm>
#include "Vehicle.h"

class Garage {
    std::vector<Vehicle*> vehicles_;
public:
    void add(Vehicle* v)
    {
        vehicles_.push_back(v);
    }
    void remove(int number)
    {
        vehicles_.erase(std::remove_if(vehicles_.begin(), vehicles_.end(), [&](const auto &item)  {return (item->getNumber() == number);}));
    }

    const Vehicle* find(std::string type)
    {
        return *std::find_if(vehicles_.begin(), vehicles_.end(), [&](Vehicle* item) {return (item->getType() == type);});
    }
    const Vehicle* find(int number)
    {
        return *std::find_if(vehicles_.begin(), vehicles_.end(), [&](Vehicle* item) {return (item->getNumber() == number);});
    }
    const Vehicle* find(float max_speed)
    {
        return *std::find_if(vehicles_.begin(), vehicles_.end(), [&](Vehicle* item) {return (item->getMaxSpeed() == max_speed);});
    }

    void sortByType()
    {
        std::sort(vehicles_.begin(), vehicles_.end(), [&](Vehicle* litem, Vehicle* ritem)->bool {return litem->getType() < ritem->getType();});
    }
    void sortBySpeed()
    {
        std::sort(vehicles_.begin(), vehicles_.end(), [&](Vehicle* litem, Vehicle* ritem)->bool {return litem->getMaxSpeed() < ritem->getMaxSpeed();});
    }
};

#endif //VEHICLES_GARAGE_H
