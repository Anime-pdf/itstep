#ifndef VEHICLES_LORRY_H
#define VEHICLES_LORRY_H

#include "Vehicle.h"

class Lorry : public Vehicle {
protected:
    float capacity_;
public:
    Lorry(const std::string& model, const std::string& manufacturer, float price, float max_speed, int year, float capacity) :
            Vehicle(model, manufacturer, price, max_speed, year) {
        capacity_ = capacity;
    }

    float getCapacity() const {return capacity_;}

    std::string getType() const override {return "Lorry";}
};

#endif //VEHICLES_LORRY_H
