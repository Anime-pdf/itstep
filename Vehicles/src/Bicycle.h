#ifndef VEHICLES_BICYCLE_H
#define VEHICLES_BICYCLE_H

#include "Vehicle.h"

class Bicycle : public Vehicle {
protected:
    bool for_children_;
public:
    Bicycle(const std::string& model, const std::string& manufacturer, float price, float max_speed, int year, bool for_children) :
    Vehicle(model, manufacturer, price, max_speed, year) {
        for_children_ = for_children;
    }

    bool isForChildren() const {return for_children_;}

    std::string getType() const override {return "Bicycle";}
};

#endif //VEHICLES_BICYCLE_H
