#ifndef VEHICLES_VEHICLE_H
#define VEHICLES_VEHICLE_H

#include <string>

class Vehicle {
protected:
    std::string manufacturer_;
    std::string model_;

    float price_;
    float max_speed_;
    int year_;
    int number_;

    Vehicle(const std::string& model, const std::string& manufacturer, float price, float max_speed, int year)
    {
        model_ = model;
        manufacturer_ = manufacturer;
        price_ = price;
        max_speed_ = max_speed;
        year_ = year;
        number_ = rand() % 10000;
    }
public:
    virtual ~Vehicle() = default;

    std::string getManufacturer() const {return manufacturer_;}
    std::string getModel() const {return model_;}
    float getPrice() const {return price_;}
    float getMaxSpeed() const {return max_speed_;}
    int getYear() const {return year_;}
    int getNumber() const {return number_;}

    virtual std::string getType() const = 0;
};

#endif //VEHICLES_VEHICLE_H
