#ifndef VEHICLES_CAR_H
#define VEHICLES_CAR_H

#include "Vehicle.h"

class Car : public Vehicle {
protected:
    float fuel_tank_;
    enum CarType {
        Type_Sport = 0,
        Type_Minivan,
        Type_Business,
    } car_type_;
public:
    Car(const std::string& model, const std::string& manufacturer, float price, float max_speed, int year, float fuel_tank, CarType type) :
    Vehicle(model, manufacturer, price, max_speed, year) {
        fuel_tank_ = fuel_tank, car_type_ = type;
    }

    float getFuelTank() const {return fuel_tank_;}
    CarType getCarType() const {return car_type_;}

    std::string getType() const override {return "Car";}
};

#endif //VEHICLES_CAR_H
