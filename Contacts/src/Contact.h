#ifndef CONTACTS_CONTACT_H
#define CONTACTS_CONTACT_H

#include <string>

class Contact {
public:
    virtual std::string Show() = 0;
    virtual std::string Type() = 0;
};

#endif //CONTACTS_CONTACT_H
