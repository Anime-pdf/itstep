#ifndef CONTACTS_FRIEND_H
#define CONTACTS_FRIEND_H

#include "Contact.h"

class Friend : public Contact {
public:
    std::string name_;
    std::string address_;
    std::string phone_;
    std::string birthday_;

    Friend() : name_("John Doe"), address_("Somewhere"), phone_("123456789"), birthday_("111222") {}
    Friend(std::string name, std::string address, std::string phone, std::string birthday) : name_(std::move(name)), address_(std::move(address)), phone_(std::move(phone)), birthday_(std::move(birthday)) {}

    std::string Show() override
    {
        return ("Name: " + name_ + "\nAddress: " + address_ + "\nPhone: " + phone_ + "\nBirthday: " + birthday_ + "\n");
    }
    std::string Type() override
    {
        return "Friend";
    }
};

#endif //CONTACTS_FRIEND_H
