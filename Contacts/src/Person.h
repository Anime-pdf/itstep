#ifndef CONTACTS_PERSON_H
#define CONTACTS_PERSON_H

#include "Contact.h"

class Person : public Contact {
public:
    std::string name_;
    std::string address_;
    std::string phone_;

    Person() : name_("John Doe"), address_("Somewhere"), phone_("123456789") {}
    Person(std::string name, std::string address, std::string phone) : name_(std::move(name)), address_(std::move(address)), phone_(std::move(phone)) {}

    std::string Show() override
    {
        return ("Name: " + name_ + "\nAddress: " + address_ + "\nPhone: " + phone_ + "\n");
    }
    std::string Type() override
    {
        return "Person";
    }
};

#endif //CONTACTS_PERSON_H
