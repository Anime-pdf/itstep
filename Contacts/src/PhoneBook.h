#ifndef CONTACTS_PHONEBOOK_H
#define CONTACTS_PHONEBOOK_H

#include <vector>
#include <iostream>
#include "Contact.h"

class PhoneBook {
    std::vector<Contact*> contacts;
public:
    void add(Contact* item)
    {
        if(item!= nullptr)
            contacts.push_back(item);
    }
    void remove(int id)
    {
        contacts.erase(contacts.begin()+id);
    }
    void print()
    {
        for (int i = 0; i < contacts.size(); ++i) {
            std::cout << std::endl << i << ": Type - " << contacts.at(i)->Type() << " \n"
            << contacts.at(i)->Show();
        }
    }
};

#endif //CONTACTS_PHONEBOOK_H
