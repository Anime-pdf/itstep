#ifndef CONTACTS_COWORKER_H
#define CONTACTS_COWORKER_H

#include "Contact.h"

class Coworker : public Contact {
public:
    std::string name_;
    std::string address_;
    std::string phone_;
    std::string fax_;
    std::string org_;

    Coworker() : name_("John Doe"), address_("Somewhere"), phone_("123456789"), fax_("111222"), org_("Unlimited Money Limited.") {}
    Coworker(std::string name, std::string address, std::string phone, std::string fax, std::string org) : name_(std::move(name)), address_(std::move(address)), phone_(std::move(phone)), fax_(std::move(fax)), org_(std::move(org)) {}

    std::string Show() override
    {
        return ("Name: " + name_ + "\nAddress: " + address_ + "\nPhone: " + phone_ + "\nFax: " + fax_ + "\nOrganisation: " + org_ + "\n");
    }
    std::string Type() override
    {
        return "Coworker";
    }
};

#endif //CONTACTS_COWORKER_H
