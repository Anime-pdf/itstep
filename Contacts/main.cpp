#include <iostream>
#include "src/PhoneBook.h"
#include "src/Friend.h"
#include "src/Person.h"
#include "src/Coworker.h"

int main() {
    PhoneBook a;
    a.add(new Friend("a0", "a", "a", "a"));
    a.add(new Person("a1", "a", "a"));
    a.add(new Coworker("a2", "a", "a", "a", "a"));

    a.print();

    a.remove(2);
    printf("\n===== remove\n");

    a.print();
    return 0;
}
