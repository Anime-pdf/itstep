#ifndef ARRAY_ARR_H
#define ARRAY_ARR_H

#include <string>
#include <algorithm>

template <typename T>
class Array{
    int m_size;
    T* m_arr;
public:
    Array(T item, int size=0) {
        m_size = size;
        m_arr = new T[size];
        for (int i = 0; i < size; ++i) {
            m_arr[i] = item;
        }
    }
    ~Array() {
        delete[] m_arr;
    }

    int size()const {
        return m_size;
    }	//возвращает размерность массива

    T front()const {
        return *(m_arr+m_size);
    }	//возвращает первый элемент массива

    T back()const {
        return *m_arr;
    }	//возвращает последний элемент массива

    void print()const {
        for (int i = 0; i < size(); ++i) {
            std::cout << "[" << i << "]" << m_arr[i] << std::endl;
        }
        std::cout << std::endl;
    }	//вывод всех элементов массива

    void push(T item) {
        T* tmp = new T[size() + 1];
        for (int i = 0; i < size(); ++i) {
            tmp[i] = m_arr[i];
        }
        tmp[m_size]=item;
        m_arr = tmp;
        m_size++;
    }	//добавление элемента item в конец массива

    void insert(T item, int pos = 0) {
        T* tmp = new T[size()+1];
        for (int i = 0, j = 0; i < size(); ++i) {
            if (i == pos)
                j++;
            tmp[j] = m_arr[i];
            j++;
        }
        tmp[pos]=item;
        m_arr = tmp;
        m_size++;
    } //добавление элемента item на заданную позицию

    void erase(int pos) {
        T* tmp = new T[size()-1];
        for (int i = 0, j = 0; i < size(); ++i) {
            if (i == pos)
                i++;
            tmp[j] = m_arr[i];
            j++;
        }
        m_arr = tmp;
        m_size--;
    }	//удаление элемента по указанной позиции

    T find_max()const {
        T max = m_arr[0];
        for (int i = 0; i < size(); ++i) {
            if(m_arr[i] > max)
                max = m_arr[i];
        }
        return max;
    } //поиск элемента с максимальным значением
    T find_min()const {
        T min = m_arr[0];
        for (int i = 0; i < size(); ++i) {
            if(m_arr[i] < min)
                min = m_arr[i];
        }
        return min;
    } //поиск элемента с минимальным значением

    void sort() {
        std::sort(m_arr, (m_arr+m_size));
    }		//отсортировать все элементы массива по возрастанию

    T& at(int k) {
        return m_arr[k];
    }		//доступ к элементу массива по позиции К
    T& operator[](int k) {
        return m_arr[k];
    } //доступ к элементу массива по позиции К

    int count_if(bool (*func)(T)) {
        int c;
        for (int i = 0; i < size(); ++i) {
            if(func(m_arr[i]))
                c++;
        }
        return c;
    }	 // подсчет количества элементов массива по условию
    int firstfind_if(bool (*func)(T)) {
        for (int i = 0; i < size(); ++i) {
            if(func(m_arr[i]))
                return i;
        }
    } //поиск позиции первого элемента массива по условию
};

#endif //ARRAY_ARR_H
