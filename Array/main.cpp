#include <iostream>
#include <src/arr.h>

int main() {
    Array<std::string> a("tt", 2);
    a.print();
    a.push("keek");
    a.print();
    a.insert("lol", 0);
    a.print();
    a.erase(1);
    a.print();
    a.sort();
    a.print();
//    Array<int> a(12, 2);
//    a.print();

    return 0;
}
