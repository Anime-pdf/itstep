#include <iostream>
#include "src/PiggyBank.h"
#include "src/Deposit.h"

int main() {
    PiggyBank p(10);
    Deposit d(Date(), 10, 10);
    Balance b;

    p.deposit(100);
    p.print();

    d.deposit(100);
    d.print();

    b.deposit(100);
    b.print();
    return 0;
}
