#ifndef BANK_DEPOSIT_H
#define BANK_DEPOSIT_H

#include <src/Date.h>
#include "Balance.h"

class Deposit : public Balance {
    Date _depDate;
    int _days;
    float _percent;
public:
    Deposit(const Date& depDate, int days, float percent) : _depDate(depDate) { setPercent(percent); setDays(days); }

    bool setDays(int days) { if(days <= 0) return false; _days = days; return true; }
    bool setPercent(float percent) { if(percent <= 0) return false; _percent = percent; return true; }

    void print() const override {
        Balance::print();
        printf("Deposit date: %s\nDeposit days: %d\nDeposit percent: %.02f\n", _depDate.returnFormatted().c_str(), _days, _percent);
    }
};

#endif //BANK_DEPOSIT_H
