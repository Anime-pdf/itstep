#ifndef BANK_PIGGYBANK_H
#define BANK_PIGGYBANK_H

#include "Balance.h"

class PiggyBank : public Balance {
    float _percent;
    float _cashback;
public:
    PiggyBank(float percent) : _cashback(0) { setPercent(percent); }

    bool setPercent(float percent) { if(percent <= 0) return false; _percent = percent; return true; }

    bool deposit(float amount) override {
        if(amount <= 0) return false;
        _cashback += amount*_percent/100;
        return true;
    }

    bool withdraw(float amount) override {
        if (amount <= 0 || amount > _cashback)
            return false;
        Balance::deposit(_cashback);
        _cashback-=amount;
        return true;
    }

    void print() const override {
        Balance::print();
        printf("Percent: %.02f\nCashback: %.02f\n", _percent, _cashback);
    }
};

#endif //BANK_PIGGYBANK_H
