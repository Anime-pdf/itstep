#ifndef BANK_BALANCE_H
#define BANK_BALANCE_H

#include <cstdio>

class Balance {
    float _cash;
public:
    Balance() : _cash(0) {}

    virtual bool withdraw(float amount) { if(amount <= 0 || amount > _cash ) return false; _cash -= amount; return true; }
    virtual bool deposit(float amount) { if(amount <= 0) return false; _cash += amount; return true; }

    virtual void print() const {
        printf("\nBalance: %.02f\n", _cash);
    }
protected:
    float getBalance() const { return _cash; }
};

#endif //BANK_BALANCE_H
