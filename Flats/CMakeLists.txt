cmake_minimum_required(VERSION 3.23)
project(hw2)

set(CMAKE_CXX_STANDARD 23)

add_executable(hw2 main.cpp src/Resident.h src/Flat.h src/Building.h)
