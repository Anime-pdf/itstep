#include <iostream>
#include "src/Resident.h"

int main() {
    Resident r("Doagsge");
    Resident a("Goe");
    FILE* file = fopen("prek.bin", "w+b");
    r.toBinFile(file);
    fclose(file);

    file = fopen("prek.bin", "r+b");
    a.fromBinFile(file);
    std::cout << a.getName() << std::endl;
    fclose(file);
    return 0;
}
