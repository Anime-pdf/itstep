#ifndef HW2_FLAT_H
#define HW2_FLAT_H

#include "Resident.h"

class Flat {
    int m_Rooms;
    float m_Square;
    int m_AmountOfResidents;
    Resident** m_Residents;
public:
    Flat(int rooms=2,float square=27) { setRooms(rooms); setSquare(square); m_AmountOfResidents=0; m_Residents= nullptr; }
    ~Flat() {delete[] m_Residents;}

    Flat(const Flat& other)
    {
        m_Rooms = other.m_Rooms;
        m_Square = other.m_Square;
        m_AmountOfResidents = other.m_AmountOfResidents;
        m_Residents = new Resident*[m_AmountOfResidents];
        for (int i = 0; i < m_AmountOfResidents; ++i) {
            m_Residents[i] = other.m_Residents[i];
        }
    }
    Flat operator=(const Flat& other)
    {
        m_Rooms = other.m_Rooms;
        m_Square = other.m_Square;
        m_AmountOfResidents = other.m_AmountOfResidents;
        m_Residents = new Resident*[m_AmountOfResidents];
        for (int i = 0; i < m_AmountOfResidents; ++i) {
            m_Residents[i] = other.m_Residents[i];
        }
    }

    int getRooms() const {return m_Rooms;}
    float getSquare() const {return m_Square;}
    int getAmmountOfResidents() const {return m_AmountOfResidents;}

    void setRooms(int rooms)  {if(rooms >0) m_Rooms = rooms;}
    void setSquare(float square) {if(square >m_Rooms) m_Square = square;}

    void addResident(Resident person) {
        Resident** tmp = new Resident*[m_AmountOfResidents+1];
        for (int i = 0; i < m_AmountOfResidents; ++i) {
            tmp[i] = m_Residents[i];
        }
        tmp[m_AmountOfResidents] = &person;
        m_AmountOfResidents++;
        m_Residents = tmp;
    }

    void delResident(char* name){
        int index = -1;
        for (int i = 0; i < m_AmountOfResidents; ++i) {
            if (!strcmp(m_Residents[i]->getName(), name))
                index = i;
        }
        Resident** tmp = new Resident*[m_AmountOfResidents - 1];
        for (int i = 0, j = 0; i < m_AmountOfResidents; ++i, ++j)
            if (i == index){j = i-1;continue;}
            else
                tmp[j] = m_Residents[i];
        m_Residents = tmp;
        m_AmountOfResidents--;
    }

    void showResidents() const {
        printf("\nResidents:\n");
        for (int i = 0; i < m_AmountOfResidents; ++i) {
            printf("[%d] Name: %s\n", i, m_Residents[i]->getName());
        }
    }

    void toBinFile(FILE* fp) {
        fwrite(&m_Rooms, sizeof(int), 1, fp);
        fwrite(&m_Square, sizeof(float), 1, fp);
        fwrite(&m_AmountOfResidents, sizeof(int), 1, fp);
        for (int i = 0; i < m_AmountOfResidents; ++i)
            m_Residents[i]->toBinFile(fp);
    }
    void fromBinFile(FILE* fp) {
        fread(&m_Rooms, sizeof(int), 1, fp);
        fread(&m_Square, sizeof(float), 1, fp);
        fread(&m_AmountOfResidents, sizeof(int), 1, fp);
        m_Residents = new Resident*[m_AmountOfResidents];
        for (int i = 0; i < m_AmountOfResidents; ++i)
            m_Residents[i]->fromBinFile(fp);
    }
};

#endif //HW2_FLAT_H