#ifndef HW2_BUILDING_H
#define HW2_BUILDING_H

#include "Flat.h"

const int MAX_FLATS = 20;

class Building
{
    char* m_Address;
    Flat m_Flats[MAX_FLATS];
public:
    Building(char* address = "Pushkin st. Kolotuskin house") { setAddress(address);}
    ~Building() {delete[] m_Address;}

    char* getAddress() const {return m_Address;}
    void setAddress(char* address) {m_Address = new char[strlen(address)+1]; strcpy(m_Address, address);}

    void addPerson(Resident person, int flat) {if (flat >= MAX_FLATS || flat < 0) return;m_Flats[flat].addResident(person);}
    void delPerson(char* name, int flat) {if (flat >= MAX_FLATS || flat < 0) return;m_Flats[flat].delResident(name);}

    void showAllFlatsList() const {
        printf("\nAddress: %s\nNumber of flats: %d\nFlats:\n", m_Address, MAX_FLATS);
        for (int i = 0; i < MAX_FLATS; ++i) {
        printf("Flat n[%d]:\nNumber of rooms: %d\nNumber of square meters: %f\nCurrent number of residents: %d\n",i,m_Flats[i].getRooms(),m_Flats[i].getSquare(),m_Flats[i].getAmmountOfResidents());
        }
    }

    void showAllPersonList() const {
        for (int i = 0; i < MAX_FLATS; ++i) {
            printf("Flat n[%d]:\n", i);
            m_Flats[i].showResidents();
        }
    }

    void showPersonListByFlat(int n) const {
        if (n >= MAX_FLATS || n < 0)
            return;
        printf("Flat n[%d]:\n", n);
        m_Flats[n].showResidents();
    }

    void toBinFile(FILE* fp) {
        int size = strlen(m_Address);
        fwrite(&size, sizeof(int), 1, fp);
        fwrite(m_Address, sizeof(char), size, fp);
        for (int i = 0; i < MAX_FLATS; ++i)
            m_Flats[i].toBinFile(fp);
    }

    void fromBinFile(FILE* fp) {
        int size;
        fread(&size, sizeof(int), 1, fp);
        fread(m_Address, sizeof(char), size, fp);
        for (int i = 0; i < MAX_FLATS; ++i)
            m_Flats[i].fromBinFile(fp);
    }
};

#endif //HW2_BUILDING_H