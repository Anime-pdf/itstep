#ifndef HW2_RESIDENT_H
#define HW2_RESIDENT_H

#include <cstring>
#include <cstdio>

class Resident {
    char* m_Name;
public:
    Resident(char* name="John") {setName(name);}
    ~Resident() {delete[] m_Name;}

    Resident(const Resident& other)
    {
        setName(other.getName());
    }
    Resident operator=(const Resident& other)
    {
        setName(other.getName());
    }

    const char* getName() const {return m_Name;}

    void setName(const char* name) {m_Name = new char[strlen(name)+1];strcpy(m_Name, name);}

    void toBinFile(FILE* fp) {
        int size = strlen(m_Name);
        fwrite(&size, sizeof(int), 1, fp);
        fwrite(m_Name, sizeof(char), size, fp);
    }
    void fromBinFile(FILE* fp) {
        int size;
        fread(&size, sizeof(int), 1, fp);
        fread(m_Name, sizeof(char), size, fp);
    }
};

#endif //HW2_RESIDENT_H