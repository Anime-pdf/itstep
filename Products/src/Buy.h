#ifndef PRODUCTS_BUY_H
#define PRODUCTS_BUY_H

#include "Product.h"

class Buy : public Product{
    float _amount;
public:
    Buy(float amount = 1, const std::string& label = "Milk", float cost = 15.f, float weight = 1.f) : Product(label, cost, weight) {
        setAmount(amount);
    }

    float getAmount() const { return _amount; };

    bool setAmount(float amount) { if(amount < 0) return false; _amount = amount; return true; }

    float calcTotalWeight() const { return _amount * getWeight(); }
    float calcTotalPrice() const { return _amount * getCost(); }

    void virtual showInfo() const {
        Product::showInfo();
        printf("Amount: %.02f\nNew Price: %.02f\nNew Weight: %.02f\n", getAmount(), calcTotalPrice(), calcTotalWeight());
    }
};

#endif //PRODUCTS_BUY_H
