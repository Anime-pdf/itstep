#ifndef PRODUCTS_PRODUCT_H
#define PRODUCTS_PRODUCT_H

#include <string>

class Product {
    float _cost;
    float _weight;
    std::string _label;
public:
    Product(const std::string& label = "Milk", float cost = 15.f, float weight = 1.f) {
        setLabel(label);
        setCost(cost);
        setWeight(weight);
    }

    float getCost() const {return _cost;}
    float getWeight() const {return _weight;}
    std::string getLabel() const {return _label;}

    bool setCost(float cost) { if(cost < 0) return false; _cost = cost; return true; }
    bool setWeight(float weight) { if(weight < 0) return false; _weight = weight; return true; }
    bool setLabel(const std::string& label) { if(label.empty()) return false; _label = label; return true; }

    virtual void showInfo() const {
        printf("Label: %s\nPrice: %.02f\nWeight: %.02f\n", getLabel().c_str(), getCost(), getWeight());
    }

};

#endif //PRODUCTS_PRODUCT_H
