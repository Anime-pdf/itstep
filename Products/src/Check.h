#ifndef PRODUCTS_CHECK_H
#define PRODUCTS_CHECK_H

#include "Buy.h"

class Check : public Buy {
public:
    Check(float amount = 1, const std::string& label = "Milk", float cost = 15.f, float weight = 1.f) : Buy(amount, label, cost, weight) {}
    void showInfo() const final {
        Buy::showInfo();
        printf("\n=== Purchase Successfully! ===\n");
    }
};

#endif //PRODUCTS_CHECK_H
