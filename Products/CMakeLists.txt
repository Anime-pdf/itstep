cmake_minimum_required(VERSION 3.23)
project(Products)

set(CMAKE_CXX_STANDARD 20)

add_executable(Products main.cpp src/Check.h src/Product.h src/Buy.h)
