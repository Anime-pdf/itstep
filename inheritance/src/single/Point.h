#ifndef INHERITANCE_POINT_H
#define INHERITANCE_POINT_H

class Point {
public: // нам не нужны аксессоры, если мы допускаем любые значения
    double x;
    double y;

    Point(double x = 0, double y = 0) : x(x), y(y) {}

    virtual double area()
    {
        return 0;
    }
};

#endif //INHERITANCE_POINT_H
