#ifndef INHERITANCE_RECTANGLE_H
#define INHERITANCE_RECTANGLE_H

#include "Point.h"

class Rectangle : public Point {
    double _height;
    double _width;
public:
    Rectangle(double x, double y, double height, double width) : Point(x,y)
    {
        if(!setHeight(height))
            setHeight(1);
        if(!setWidth(width))
            setWidth(1);
    }

    double getHeight() const {return _height;}
    double getWidth() const {return _width;}

    bool setHeight(double h)
    {
        if(h <= 0)
            return false;
        _height = h;
        return true;
    }

    bool setWidth(double w)
    {
        if(w <= 0)
            return false;
        _width = w;
        return true;
    }

    double area() override
    {
        return _height * _width;
    }
};

#endif //INHERITANCE_RECTANGLE_H
