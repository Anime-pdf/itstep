#ifndef INHERITANCE_CIRCLE_H
#define INHERITANCE_CIRCLE_H

#include "Point.h"

#define pi 3.14159265358979323846

class Circle : Point {
    double _radius;
public:
    Circle(double x, double y, double radius) : Point(x,y)
    {
        if(!setRadius(radius))
            setRadius(1);
    }

    double getRadius() const {return _radius;}

    bool setRadius(double r)
    {
        if(r <= 0)
            return false;
        _radius = r;
        return true;
    }

    double area() override
    {
        return pi * _radius * _radius;
    }
};

#endif //INHERITANCE_CIRCLE_H
