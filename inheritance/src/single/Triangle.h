#ifndef INHERITANCE_TRIANGLE_H
#define INHERITANCE_TRIANGLE_H

#include <cmath>
#include "Point.h"

class Triangle : Point {
    double _side1;
    double _side2;
    double _angle;
public:
    Triangle(double x, double y, double side1, double side2, double angle) : Point(x,y)
    {
        if(!setSide1(side1))
            setSide1(1);
        if(!setSide2(side2))
            setSide2(1);
        if(!setAngle(angle))
            setAngle(60);
    }

    double getSide1() const {return _side1;}
    double getSide2() const {return _side2;}
    double getAngle() const {return _angle;}

    bool setSide1(double s)
    {
        if(s <= 0)
            return false;
        _side1 = s;
        return true;
    }

    bool setSide2(double s)
    {
        if(s <= 0)
            return false;
        _side2 = s;
        return true;
    }

    bool setAngle(double a)
    {
        if(a <= 0 || a >= 180)
            return false;
        _angle = a;
        return true;
    }

    double area() override
    {
        return 0.5f * _side1 * _side2 * std::sin(_angle);
    }
};

#endif //INHERITANCE_TRIANGLE_H
