#ifndef INHERITANCE_AUDIO_BOOK_H
#define INHERITANCE_AUDIO_BOOK_H

#include "Voice_record.h"
#include "Book.h"

class Audio_book : public Voice_record, public Book{
    int Tapes;
public:
    Audio_book(std::string speaker_, int duration_, const Date& date, std::string author_, std::string edition_, const Date& dateB, long isbn_, int tapes) : Voice_record(speaker_, duration_, date),
                                                                                                                                                  Book(author_, edition_, dateB, isbn_), Tapes(tapes) {}
};

#endif //INHERITANCE_AUDIO_BOOK_H
