#ifndef INHERITANCE_VOICE_RECORD_H
#define INHERITANCE_VOICE_RECORD_H

#include <string>
#include <utility>
#include "src/Date.h"

class Voice_record {
public:
    std::string speaker;
    int duration;
    Date recording_date;
    Voice_record(std::string speaker_, int duration_, const Date& date) : speaker(std::move(speaker_)), duration(duration_), recording_date(date) {}
};

#endif //INHERITANCE_VOICE_RECORD_H
