#ifndef INHERITANCE_BOOK_H
#define INHERITANCE_BOOK_H

#include <string>
#include <utility>
#include "src/Date.h"

class Book {
public:
    std::string author;
    std::string edition;
    Date pub_date;
    long isbn;
    Book(std::string author_, std::string edition_, const Date& date, long isbn_) : author(std::move(author_)), edition(std::move(edition_)), pub_date(date), isbn(isbn_) {}
};

#endif //INHERITANCE_BOOK_H
