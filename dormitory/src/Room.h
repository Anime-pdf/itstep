#ifndef DORMITORY_ROOM_H
#define DORMITORY_ROOM_H

class Room
{
    int m_Number;
    int m_Places;
    int m_FreePlaces;

    void setFreePlaces(int places)
    {
        if(m_Places < places || places < 0)
            return;
        m_FreePlaces = places;
    }
public:
    Room(int number = 1, int places = 4) {
        if(number <=0)
            return;
        setPlaces(places);
        m_FreePlaces = m_Places;
    }

    void setPlaces(int places)
    {
        if(places <= 0)
            places = 1;

        m_Places = places;
    }

    int getNumber() const { return m_Number; }
    int getPlaces() const { return m_Places; }
    int getFreePlaces() const { return m_FreePlaces; }

    Room& operator -- ();
    Room operator -- (int n);
    Room& operator ++ ();
    Room operator ++ (int n);

};

Room &Room::operator--() {
    setFreePlaces(m_FreePlaces-1);
    return *this;
}

Room Room::operator--(int n) {
    Room tmp = *this;
    setFreePlaces(m_FreePlaces-1);
    return tmp;
}

Room &Room::operator++() {
    setFreePlaces(m_FreePlaces+1);
    return *this;
}

Room Room::operator++(int n) {
    Room tmp = *this;
    setFreePlaces(m_FreePlaces+1);
    return tmp;
}

#endif //DORMITORY_ROOM_H
