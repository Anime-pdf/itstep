#ifndef DORMITORY_STUDENT_H
#define DORMITORY_STUDENT_H

#include <string>
#include <src/Date.h>
using std::string;

class Student {
    string m_Name;
    string m_Surname;
    Date m_Birthday;
    int m_Course;
    int m_Group;
    int m_Room;
public:
    Student(string name = "John", string surname = "Doe", const Date& date = Date(), int course = 1, int group = 11, int room = 0) :
    m_Name(name), m_Surname(surname), m_Birthday(date) {
        setCourse(course);
        setGroup(group);
        setRoom(room);
    }

    void setName(string name) { m_Name = name; }
    void setSurname(string name) { m_Surname = name; }
    void setBirthday(const Date& date) { m_Birthday = date; }
    void setCourse(int course) { if(course < 0 || course > 5) return; m_Course = course; }
    void setGroup(int group) { if(group <= 0) return; m_Group = group; }
    void setRoom(int room) { if(room <= 0) return; m_Room = room; }

    string getName() { return m_Name; }
    string getSurname() { return m_Surname; }
    Date getBirthday() { return m_Birthday; }
    int getCourse() const { return m_Course; }
    int getGroup() const { return m_Group; }
    int getRoom() const { return m_Room; }
};

#endif //DORMITORY_STUDENT_H
