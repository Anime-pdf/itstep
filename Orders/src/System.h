#ifndef ORDERS_SYSTEM_H
#define ORDERS_SYSTEM_H

#include <vector>
#include <algorithm>
#include "Order.h"

class System {
    std::vector<Order> m_Orders; // динамический массив, который хранит заказы (Order)
public:

    void addOrder(Order order)
    {
        m_Orders.push_back(order);
    }

    void printAll()
    {
        for (int i =0; i< m_Orders.size(); i++) {
            m_Orders[i].print();
        }
    }

    void processOrder(int id)
    {
        for (int i =0; i< m_Orders.size(); i++) {
            if(m_Orders[i].getId() == id)
            {
                std::cout << "Take order #" << id << std::endl << m_Orders[i].getDescription() << std::endl;
                m_Orders.erase(m_Orders.begin()+i);
                break;
            }
        }
    }

    void sortByTime()
    {
        std::sort(m_Orders.begin(), m_Orders.end(), [](Order one, Order second){return one.getTime() < second.getTime();});
    }

};

#endif //ORDERS_SYSTEM_H
