#ifndef ORDERS_ORDER_H
#define ORDERS_ORDER_H

#include <src/Date.h>
#include <src/Time.h>

class Order {
    static inline int m_Total{0};

    int m_ID;
    std::string m_Description;
    float m_Price;
    Date m_Date;
    Time m_Time;
public:
    Order(std::string description, float price, const Date& date = Date(), const Time& time = Time()) : m_ID(++m_Total), m_Description(description), m_Date(date), m_Time(time) { setPrice(price); }

    bool setPrice(float price)
    {
        if(price < 0)
            return (m_Price = 0);
        m_Price = price;
    }

    int getId() {return m_ID;}
    std::string getDescription() {return m_Description;}

    Time getTime() {return m_Time;}
    Date getDate() {return m_Date;}

    void print()
    {
        printf("Order #%d\nDate: %s %s\nPrice: %.2f\nDescription: %s\n", m_ID, m_Date.returnFormatted().c_str(), m_Time.returnFormatted().c_str(), m_Price, m_Description.c_str());
    }
};

#endif //ORDERS_ORDER_H