#include <iostream>
#include "src/System.h"

int main() {
    System a;
    int menu;
    std::string str;
    int num;
    do {
        std::cout << "1: Create order" << std::endl << "2: Print all orders" << std::endl << "3: Take order" << std::endl;
        std::cin >> menu;
        switch (menu) {
            case 1:
                std::cout << "Enter Description:";
                std::cin >> str;
                std::cout << "Enter Price:";
                std::cin >> num;
                a.addOrder(Order(str,num));
                break;
            case 2:
                a.sortByTime();
                a.printAll();
                break;
            case 3:
                std::cout << "Enter Order Id:";
                std::cin >> num;
                a.processOrder(num);
                break;
        }
    } while (menu != 0);
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
