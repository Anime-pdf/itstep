#ifndef TWORK2_BOOK_H
#define TWORK2_BOOK_H

#include "Item.h"

class Book : public Item{
    std::string m_Publishing;
public:
    Book() : m_Publishing("Unknown"), Item("C++ for dummies", 3, "CoolThingsTM", 15) {}
    Book(const std::string& name, float price, const std::string& author, int age, std::string  publishing) : m_Publishing(std::move(publishing)),
                                                                                                            Item(name, price, author, age) {}
    void Show() const override
    {
        printf("Type: %s\nName: %s\nPrice: $%0.2f\nAuthor: %s\nRecommended age: %d\nPublisher: %s\n", Type().c_str(), m_Name.c_str(), m_Price, m_Author.c_str(), m_Age, m_Publishing.c_str());
    }
    std::string Type() const override
    {
        return "Book";
    }

    void Fuzz() override
    {
        Item::Fuzz();
        std::replace(m_Publishing.begin(), m_Publishing.end(),' ', '_');
    }
    void UnFuzz() override
    {
        Item::UnFuzz();
        std::replace(m_Publishing.begin(), m_Publishing.end(),'_', ' ');
    }

    void Save(std::ofstream& file) const override
    {
        if(file.is_open())
            file << m_Name << " " << m_Price << " " << m_Author << " " << m_Age << " " << m_Publishing << std::endl;
    }
    void Load(std::ifstream& file) override
    {
        if(file.is_open())
            file >> m_Name >> m_Price >> m_Author >> m_Age >> m_Publishing;
    }
};

#endif //TWORK2_BOOK_H
