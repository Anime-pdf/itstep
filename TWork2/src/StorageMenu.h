#ifndef TWORK2_STORAGEMENU_H
#define TWORK2_STORAGEMENU_H

#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
#include "Item.h"
#include "Book.h"
#include "SportEquipment.h"
#include "Toy.h"

class StorageMenu {
    inline static int m_NextId = 0;
    std::map<int, Item*> m_Items;

    bool Exists(int id)
    {
        return m_Items.find(id) != m_Items.end();
    }
public:

    ~StorageMenu()
    {
        for(std::map<int, Item*>::iterator itr = m_Items.begin(); itr != m_Items.end(); itr++)
        {
            delete (itr->second);
        }
        m_Items.clear();
    }

    void Add(Item* item)
    {
        m_Items.insert({m_NextId,item}); m_NextId++;
    }
    bool Remove(int id)
    {
        std::map<int, Item*>::iterator itr;
        if ((itr = m_Items.find(id)) != m_Items.end()) {
            delete itr->second;
            m_Items.erase(itr);
            return true;
        }
        return false;
    }
    bool RemoveByNameAndType(const std::string& type, const std::string& name)
    {
        for (const auto &item: m_Items)
        {
            if(item.second->Type() == type || item.second->GetName() == name)
                return Remove(item.first);
        }
        return false;
    }
    bool RemoveByAuthorAndType(const std::string& type, const std::string& author)
    {
        for (const auto &item: m_Items)
        {
            if(item.second->Type() == type || item.second->GetAuthor() == author)
                return Remove(item.first);
        }
        return false;
    }
    bool RemoveByAgeAndType(const std::string& type, int Age)
    {
        for (const auto &item: m_Items)
        {
            if(item.second->Type() == type || item.second->GetAge() == Age)
                return Remove(item.first);
        }
        return false;
    }
    bool RemoveByPriceAndType(const std::string& type, float price)
    {
        for (const auto &item: m_Items)
        {
            if(item.second->Type() == type || item.second->GetPrice() == price)
                return Remove(item.first);
        }
        return false;
    }

    bool Replace(int id, Item* newItem)
    {
        if (Exists(id)) {
            delete m_Items.find(id)->second;
            m_Items.find(id)->second = newItem;
            return true;
        }
        return false;
    }
    bool EditName(int id, const std::string& name)
    {
        if (Exists(id)) {
            m_Items.find(id)->second->SetName(name);
            return true;
        }
        return false;
    }
    bool EditAuthor(int id, const std::string& author)
    {
        if (Exists(id)) {
            m_Items.find(id)->second->SetAuthor(author);
            return true;
        }
        return false;
    }
    bool EditAge(int id, int age)
    {
        if (Exists(id)) {
            m_Items.find(id)->second->SetAge(age);
            return true;
        }
        return false;
    }
    bool EditPrice(int id, float price)
    {
        if (Exists(id)) {
            m_Items.find(id)->second->SetPrice(price);
            return true;
        }
        return false;
    }

    std::vector<std::pair<int, Item*>> GetSortedById()
    {
        std::vector<std::pair<int, Item*>> items;
        for (const auto &item: m_Items)
            items.emplace_back(item);
        return items;
    }
    std::vector<std::pair<int, Item*>> GetSortedByType()
    {
        std::vector<std::pair<int, Item*>> items = GetSortedById();
        std::sort(items.begin(), items.end(), [](std::pair<int, Item*> pair1, std::pair<int, Item*> pair2) -> bool {return pair1.second->Type() < pair2.second->Type();});
        return items;
    }
    std::vector<std::pair<int, Item*>> GetSortedByAge()
    {
        std::vector<std::pair<int, Item*>> items = GetSortedById();
        std::sort(items.begin(), items.end(), [](std::pair<int, Item*> pair1, std::pair<int, Item*> pair2) -> bool {return pair1.second->GetAge() < pair2.second->GetAge();});
        return items;
    }
    std::vector<std::pair<int, Item*>> GetSortedByPrice()
    {
        std::vector<std::pair<int, Item*>> items = GetSortedById();
        std::sort(items.begin(), items.end(), [](std::pair<int, Item*> pair1, std::pair<int, Item*> pair2) -> bool {return pair1.second->GetPrice() < pair2.second->GetPrice();});
        return items;
    }
    std::vector<std::pair<int, Item*>> GetSortedByName()
    {
        std::vector<std::pair<int, Item*>> items = GetSortedById();
        std::sort(items.begin(), items.end(), [](std::pair<int, Item*> pair1, std::pair<int, Item*> pair2) -> bool {return pair1.second->GetName() < pair2.second->GetName();});
        return items;
    }
    std::vector<std::pair<int, Item*>> GetSortedByAuthor()
    {
        std::vector<std::pair<int, Item*>> items = GetSortedById();
        std::sort(items.begin(), items.end(), [](std::pair<int, Item*> pair1, std::pair<int, Item*> pair2) -> bool {return pair1.second->GetAuthor() < pair2.second->GetAuthor();});
        return items;
    }

    void ShowAllItems()
    {
        for (const auto &item: m_Items)
        {
            printf("==== Item: [%d]\n", item.first);
            item.second->Show();
        }
    }

    void Save(std::ofstream& file) const
    {
        if (file.is_open()) {
            file << m_Items.size() << std::endl;
            for (const auto &item: m_Items) {
                item.second->Fuzz();
                file << item.second->Type() << " " << item.first << " ";
                item.second->Save(file);
            }
            file << m_NextId;
        }
    }
    void Load(std::ifstream& file)
    {
        if (file.is_open()) {
            size_t size;
            int id;
            std::string type;
            file >> size;
            for (int i = 0; i < size; ++i) {
                file >> type >> id;
                if(type == "Book")
                    m_Items.emplace(id, new Book());
                else if(type == "Toy")
                    m_Items.emplace(id, new Toy());
                else if(type == "SportEquipment")
                    m_Items.emplace(id, new SportEquipment());
                (*m_Items.find(id)).second->Load(file);
                (*m_Items.find(id)).second->UnFuzz();
            }
            file >> m_NextId;
        }
    }

};

#endif //TWORK2_STORAGEMENU_H
