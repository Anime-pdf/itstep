#ifndef TWORK2_TOY_H
#define TWORK2_TOY_H

#include "Item.h"

class Toy : public Item{
    std::string m_Material;
public:
    Toy() : m_Material("Unknown"), Item("Doll", 5, "CoolThingsTM", 5) {}
    Toy(const std::string& name, float price, const std::string& author, int age, std::string  material) : m_Material(std::move(material)),
                                                                                                           Item(name, price, author, age) {}
    void Show() const override
    {
        printf("Type: %s\nName: %s\nPrice: $%0.2f\nManufacturer: %s\nRecommended age: %d\nMaterial: %s\n", Type().c_str(), m_Name.c_str(), m_Price, m_Author.c_str(), m_Age, m_Material.c_str());
    }
    std::string Type() const override
    {
        return "Toy";
    }

    void Fuzz() override
    {
        Item::Fuzz();
        std::replace(m_Material.begin(), m_Material.end(),' ', '_');
    }
    void UnFuzz() override
    {
        Item::UnFuzz();
        std::replace(m_Material.begin(), m_Material.end(),'_', ' ');
    }

    void Save(std::ofstream& file) const override
    {
        if(file.is_open())
            file << m_Name << " " << m_Price << " " << m_Author << " " << m_Age << " " << m_Material << std::endl;
    }
    void Load(std::ifstream& file) override
    {
        if(file.is_open())
            file >> m_Name >> m_Price >> m_Author >> m_Age >> m_Material;
    }
};

#endif //TWORK2_TOY_H
