#ifndef TWORK2_SPORTEQUIPMENT_H
#define TWORK2_SPORTEQUIPMENT_H

#include "Item.h"

class SportEquipment : public Item {
    std::string m_SportType;
public:
    SportEquipment() : m_SportType("Football"), Item("FootBall ball", 6, "CoolThingsTM", 15) {}
    SportEquipment(const std::string& name, float price, const std::string& author, int age, std::string  publishing) : m_SportType(std::move(publishing)),
                                                                                                                        Item(name, price, author, age) {}
    void Show() const override
    {
        printf("Type: %s\nName: %s\nPrice: $%0.2f\nAuthor: %s\nRecommended age: %d\nSport Type: %s\n", Type().c_str(), m_Name.c_str(), m_Price, m_Author.c_str(), m_Age, m_SportType.c_str());
    }
    std::string Type() const override
    {
        return "SportEquipment";
    }

    void Fuzz() override
    {
        Item::Fuzz();
        std::replace(m_SportType.begin(), m_SportType.end(),' ', '_');
    }
    void UnFuzz() override
    {
        Item::UnFuzz();
        std::replace(m_SportType.begin(), m_SportType.end(),'_', ' ');
    }

    void Save(std::ofstream& file) const override
    {
        if(file.is_open())
            file << m_Name << " " << m_Price << " " << m_Author << " " << m_Age << " " << m_SportType << std::endl;
    }
    void Load(std::ifstream& file) override
    {
        if(file.is_open())
            file >> m_Name >> m_Price >> m_Author >> m_Age >> m_SportType;
    }
};

#endif //TWORK2_SPORTEQUIPMENT_H
