#ifndef TWORK2_ITEM_H
#define TWORK2_ITEM_H

#include <string>
#include <utility>
#include <fstream>

class Item {
protected:
    std::string m_Name;
    float m_Price;
    std::string m_Author;
    int m_Age;
public:
    Item(std::string  name, float price, std::string  author, int age) : m_Name(std::move(name)), m_Author(std::move(author))
    {
        if(!SetAge(age))
            SetAge(0);
        if(!SetPrice(price))
            SetPrice(1);
    }
    std::string GetName() const {return m_Name;}
    std::string GetAuthor() const {return m_Author;}
    int GetAge() const {return m_Age;}
    float GetPrice() const {return m_Price;}

    void SetName(std::string n) {m_Name = std::move(n);}
    void SetAuthor(std::string a) {m_Author = std::move(a);}
    bool SetAge(int a) {if(a < 0 || a >= 100) return false; m_Age = a; return true;}
    bool SetPrice(float p) {if(p <= 0) return false; m_Price = p; return true;}

    virtual void Fuzz()
    {
        std::replace(m_Name.begin(), m_Name.end(),' ', '_');
        std::replace(m_Author.begin(), m_Author.end(),' ', '_');
    }
    virtual void UnFuzz()
    {
        std::replace(m_Name.begin(), m_Name.end(),'_', ' ');
        std::replace(m_Author.begin(), m_Author.end(),'_', ' ');
    }

    virtual ~Item() = default;
    virtual void Show() const = 0;
    virtual std::string Type() const = 0;
    virtual void Save(std::ofstream& file) const = 0;
    virtual void Load(std::ifstream& file) = 0;
};

#endif //TWORK2_ITEM_H
