#include <iostream>
#include "src/StorageMenu.h"
#include "src/Book.h"
#include "src/Toy.h"
#include "src/SportEquipment.h"

//void operator delete(void* memory, size_t size)
//{
//    printf("Freeing %zu bytes\n", size);
//    free(memory);
//}
//void* operator new(size_t size)
//{
//    printf("Allocating %zu bytes\n", size);
//    return malloc(size);
//}

int main() {
    {
        StorageMenu m;
        m.Add(new Book());
        m.Add(new Toy());
        m.Add(new SportEquipment());
        m.Add(new Book());
        m.Add(new Toy());
        m.Add(new SportEquipment());
        m.Add(new Book());
        m.Add(new Toy());
        m.Add(new SportEquipment());
        m.Add(new Book());
        m.Add(new Toy());
        m.Add(new SportEquipment());
        m.Add(new Book());
        m.Add(new Toy());
        m.Add(new SportEquipment());
        m.Add(new Book());
        m.Add(new Toy());
        m.Add(new SportEquipment());

        m.Remove(1);

        m.ShowAllItems();

        std::ofstream a("txt");

        m.Save(a);
    }
    {
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");

        StorageMenu m;
        std::ifstream a("txt");

        m.Load(a);
        m.ShowAllItems();
    }
//    for (const auto &item: m.GetSortedByType())
//        printf("id: %d type: %s\n", item.first, item.second->Type().c_str());
//    printf("\n");
    return 0;
}
