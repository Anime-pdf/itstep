#ifndef TAXI_CONTROLLER_H
#define TAXI_CONTROLLER_H

#include <list>
#include <iostream>
#include "Taxi.h"

class Controller {
    std::list<Taxi> taxis_;
public:
    void print()
    {
        for (const auto &item: taxis_) {
            std::cout << item.getNumber() << " - " << item.getModel() << std::endl;
            std::cout << "Driver: " << item.getDriver() << std::endl;
            std::cout << "Free" << " - " << item.isFree() << std::endl;
        }
    }
    void print(bool free)
    {
        for (const auto &item: taxis_) {
            if(free != item.isFree())
                continue;
            std::cout << item.getNumber() << " - " << item.getModel() << std::endl;
            std::cout << "Driver: " << item.getDriver() << std::endl;
            std::cout << "Free" << " - " << item.isFree() << std::endl;
        }
    }

    void insert(int id, const Taxi& taxi)
    {
        auto i = taxis_.begin();
        for (int j = 0; j < id; ++j)
            i++;
        taxis_.insert(i, taxi);
    }

    void insert(const Taxi& taxi)
    {
        taxis_.insert(--taxis_.end(), taxi);
    }

    void setFree(int n)
    {
        std::find_if(taxis_.begin(), taxis_.end(), [&](const Taxi &item) {
            return item.getNumber() == n;
        })->setFree(true);
    }
    void setBusy(int n)
    {
        std::find_if(taxis_.begin(), taxis_.end(), [&](const Taxi &item) {
            return item.getNumber() == n;
        })->setFree(false);
    }
};

#endif //TAXI_CONTROLLER_H
