#ifndef TAXI_TAXI_H
#define TAXI_TAXI_H

#include <string>
#include <utility>

class Taxi {
    int number_;
    std::string model_;
    std::string driver_;
    bool free_;
public:
    Taxi(int number, std::string model, std::string driver, bool free){
        number_ = number;
        model_ = std::move(model);
        driver_ = std::move(driver);
        free_ = free;
    }

    int getNumber() const {return number_;}
    std::string getModel() const {return model_;}
    std::string getDriver() const {return driver_;}
    bool isFree() const {return free_;}

    void setFree(bool f) { free_ = f; }
};

#endif //TAXI_TAXI_H
