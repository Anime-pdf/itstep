#include <iostream>
#include "src/System.h"

int main() {
    System a;
    a.takeNumber(System::AX, 1337);
    a.printNextTenNumbers(System::AX, 1332);
    return 0;
}
