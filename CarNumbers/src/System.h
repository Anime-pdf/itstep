#ifndef CARNUMBERS_SYSTEM_H
#define CARNUMBERS_SYSTEM_H

#include <array>
#include <bitset>

class System {
public:
    enum Codes {
        AK=0,
        AB,
        AC,
        AE,
        AH,
        AM,
        AO,
        AP,
        AT,
        AA,
        AI,
        BA,
        BB,
        BC,
        BE,
        BH,
        BI,
        BK,
        CH,
        BM,
        BO,
        AX,
        BT,
        BX,
        CA,
        CB,
        CE,

        II,

        CODES_NUM
    };
private:
    static std::string toString(Codes code)
    {
        switch (code) {
            case AK: return "AK";
            case AB: return "AB";
            case AC: return "AC";
            case AE: return "AE";
            case AH: return "AH";
            case AM: return "AM";
            case AO: return "AO";
            case AP: return "AP";
            case AT: return "AT";
            case AA: return "AA";
            case AI: return "AI";
            case BA: return "BA";
            case BB: return "BB";
            case BC: return "BC";
            case BE: return "BE";
            case BH: return "BH";
            case BI: return "BI";
            case BK: return "BK";
            case CH: return "CH";
            case BM: return "BM";
            case BO: return "BO";
            case AX: return "AX";
            case BT: return "BT";
            case BX: return "BX";
            case CA: return "CA";
            case CB: return "CB";
            case CE: return "CE";
            case II: return "II";
            default:
                break;
        }
    }
    std::array<std::bitset<10000>, CODES_NUM> m_Numbers;
public:
    bool takeNumber(Codes code, int number)
    {
        if (isNumberTaken(code,number))
            return false;
        m_Numbers[code].set(number, true);
        return true;
    }

    bool isNumberTaken(Codes code, int number) const
    {
        return m_Numbers[code].test(number);
    }

    void printNextTenNumbers(Codes code, int number) const
    {
        for (int i = 0; i < 10; ++i) {
            printf("%04d%s - %s\n", number+i, toString(code).c_str(), isNumberTaken(code,number+i) ? "Taken" : "Free");
        }
    }
};

#endif //CARNUMBERS_SYSTEM_H