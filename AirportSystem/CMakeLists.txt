cmake_minimum_required(VERSION 3.25)
project(AirportSystem)

set(CMAKE_CXX_STANDARD 17)

add_executable(AirportSystem main.cpp src/Controller.h)
target_include_directories(AirportSystem PRIVATE ../Date)

add_subdirectory(../Date subproject/Date)
target_link_libraries(AirportSystem Date)