#ifndef AIRPORTSYSTEM_CONTROLLER_H
#define AIRPORTSYSTEM_CONTROLLER_H

#include <string>
#include <src/Date.h>
#include <set>
#include <sstream>
#include <algorithm>

class Controller {
public:
    class Ticket {
        static int counter_;
        const int id_;
        std::string dest_;
        std::string passenger_;
        Date date_;
    public:
        Ticket(const std::string& dest, const std::string& passenger, const Date& date) : id_(counter_++)
        {
            dest_ = dest;
            passenger_ = passenger;
            date_ = date;
        }

        bool operator < (const Ticket& r) const
        {
            return ((date_ < r.date_ && dest_ < r.dest_));
        }

        std::string asString() const {
            std::stringstream out;
            out << "Ticket #" << getId() << std::endl;
            std::cout << "Destination: " << getDest() << std::endl;
            std::cout << "Passenger: " << getPassenger() << std::endl;
            std::cout << "Date: " << getDate() << std::endl;
            return out.str();
        }

        int getId() const {return id_;}
        std::string getDest() const {return dest_;}
        std::string getPassenger() const {return passenger_;}
        Date getDate() const {return date_;}
    };
private:
    std::multiset<Ticket> tickets_;

public:
    void add(const Ticket& ticket)
    {
        tickets_.insert(ticket);
    }
    void remove(int id)
    {
        tickets_.erase(std::find_if(tickets_.begin(), tickets_.end(), [&](const Ticket &item) {
            return (item.getId() == id);
        }));
    }

    int ticketsBetween(const std::string& dest, const Date& from, const Date& to)
    {
        return std::count_if(tickets_.begin(), tickets_.end(), [&](const Ticket &item) {
            return (item.getDest() == dest && item.getDate().isBetween(from, to));
        });
    }

    void print(const std::string& dest)
    {
        for (const auto &item: tickets_)
        {
            if (item.getDest() == dest)
                std::cout << item.asString();
        }
    }
    void print()
    {
        for (const auto &item: tickets_)
        {
            std::cout << item.asString();
        }
    }

};

int Controller::Ticket::counter_ = 0;

#endif //AIRPORTSYSTEM_CONTROLLER_H
