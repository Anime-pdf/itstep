#include <iostream>
#include "src/Controller.h"

int main() {
    Controller cl;

    cl.add(Controller::Ticket("Paris", "John Doe", Date(2023, 4, 2)));
    cl.add(Controller::Ticket("Paris", "Mikle L", Date(2023, 5, 2)));
    cl.add(Controller::Ticket("London", "John Doe", Date(2023, 4, 5)));
    cl.add(Controller::Ticket("Paris", "Jssoe", Date(2023, 3, 22)));

    std::cout << "-- Paris: " << std::endl;
    cl.print("Paris");
    std::cout << "-- All: " << std::endl;
    cl.print();
    std::cout << std::endl << "Tickets on paris between 2022/03/20 and 2023/02/10 : " << cl.ticketsBetween("Paris", Date(2023, 3, 20), Date(2023, 4, 10)) << std::endl;
    return 0;
}
