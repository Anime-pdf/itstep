#ifndef BUS_H
#define BUS_H

#include <src/Time.h>
/*
 * Известно время начала и окончания (например, 6:00 и 11.00) работы некоторого пригородного автобусного маршрута с одним автобусом на линии,
    а также протяженность маршрута в минутах (в один конец) = 30
    и время отдыха на конечных остановках. = 10
    Составить суточное расписание этого маршрута (моменты отправления с конечных пунктов).
 */

class Bus {
    Time m_Start;
    Time m_End;
    int m_RoutTime;
    int m_LastStop;

public:
    explicit Bus(const Time& start = Time(6,0,0,Time::TwentyFour), const Time& end = Time(11,0,0,Time::TwentyFour) + 3600, int rout = 30, int stopDelay = 10) : m_Start(start), m_End(end)
    {
        m_RoutTime = rout;
        m_LastStop = stopDelay;
    };

    void printSchedule()
    {
        int oneSideRoute = m_RoutTime+m_LastStop;
        int totalDayTime = m_End.asMinutes() - m_Start.asMinutes();
        int totalRoutes = (totalDayTime+m_LastStop)/oneSideRoute;
        Time curTime = m_Start;
        for (int i = 0; i < totalRoutes; ++i) {
            std::cout << curTime << " - " << (curTime += oneSideRoute*60) - m_LastStop*60 << std::endl;
        }
    }
};

#endif // BUS_H