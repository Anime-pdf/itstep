cmake_minimum_required(VERSION 3.23)
project(Bus)

set(CMAKE_CXX_STANDARD 20)

add_executable(Bus main.cpp src/Bus.cpp src/Bus.h)
target_include_directories(Bus PRIVATE ../Time)

add_subdirectory(../Time subproject/Time)
target_link_libraries(Bus Time)
