#include <iostream>
#include "src/Bus.h"

int main() {
    Bus a(Time(6,0,0,Time::TwentyFour), Time(11,10,0,Time::TwentyFour));
    a.printSchedule();
    return 0;
}
