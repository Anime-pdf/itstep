#include <iostream>
#include "src/MyClass.h"
#include "src/MyClass2.h"
#include "src/OutsideClass.h"
#include "src/Class1.h"
#include "src/Class2.h"
#include "src/Class3.h"
#include "src/CompanyProvider/SPlan.h"

int main() {
    MyNs::MyClass::show();
    MyNNs::MyClass::show();

    CNs::Class1 c1;
    CNs::Class2 c2;
    CNs::Class3 c3;
    OutsideClass oc;
    c1.show();
    c2.show();
    c3.show();
    oc.show();

    Company::SPlan give_me_a_name("Cool", -2);
    Company::SPlan give_me_a_name2("", 4);
    return 0;
}
