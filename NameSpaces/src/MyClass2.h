#ifndef NAMESPACES_MYCLASS2_H
#define NAMESPACES_MYCLASS2_H

#include <cstdio>

namespace MyNNs {

    class MyClass {
    public:
        static void show()
        {
            printf("My class from MyNNs\n");
        }
    };

} // MyNNs

#endif //NAMESPACES_MYCLASS2_H
