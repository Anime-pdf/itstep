//
// Created by paul on 26/12/22.
//

#ifndef NAMESPACES_CLASS3_H
#define NAMESPACES_CLASS3_H

#include "Class1.h"

namespace CNs {

    class Class3 : public Class1 {
    public:
        void show() override
        {
            printf("Class3\n");
        }
    };

} // CNs

#endif //NAMESPACES_CLASS3_H
