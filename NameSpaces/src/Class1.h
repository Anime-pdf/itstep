#ifndef NAMESPACES_CLASS1_H
#define NAMESPACES_CLASS1_H

#include <cstdio>

namespace CNs {

    class Class1 {
    public:
        virtual void show()
        {
            printf("Class1\n");
        }
    };

} // CNs

#endif //NAMESPACES_CLASS1_H
