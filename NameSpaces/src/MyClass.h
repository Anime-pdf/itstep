#ifndef NAMESPACES_MYCLASS_H
#define NAMESPACES_MYCLASS_H

#include <cstdio>

namespace MyNs {

    class MyClass {
    public:
        static void show()
        {
            printf("My class from MyNs\n");
        }

    };

} // MyNs

#endif //NAMESPACES_MYCLASS_H
