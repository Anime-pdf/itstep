#ifndef NAMESPACES_OUTSIDECLASS_H
#define NAMESPACES_OUTSIDECLASS_H

#include "Class1.h"

class OutsideClass : public CNs::Class1 {
public:
    void show() override
    {
        printf("OutsideClass\n");
    }
};

#endif //NAMESPACES_OUTSIDECLASS_H
