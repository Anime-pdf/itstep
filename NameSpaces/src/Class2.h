#ifndef NAMESPACES_CLASS2_H
#define NAMESPACES_CLASS2_H

#include "Class1.h"

namespace CNs {

    class Class2 : public Class1 {
    public:
        void show() override
        {
            printf("Class2\n");
        }
    };

} // CNs

#endif //NAMESPACES_CLASS2_H
