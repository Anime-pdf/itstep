#ifndef NAMESPACES_PROVIDER_H
#define NAMESPACES_PROVIDER_H

#include <string>
#include <utility>
#include <vector>
#include "Plan.h"

namespace Company {

    class Provider final {
        std::string m_Name;
        std::vector<Plan> m_Plans;
    public:
        Provider() : m_Name("Kyivstar") {}
        Provider(std::string name) : m_Name(std::move(name)) {}
        std::string GetName() const {return m_Name;}
        void SetName(std::string str) {m_Name = std::move(str);}
        int GetNumOfPlans() const {return m_Plans.size();}
    };

} // Company

#endif //NAMESPACES_PROVIDER_H
