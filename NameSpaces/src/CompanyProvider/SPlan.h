#ifndef NAMESPACES_SPLAN_H
#define NAMESPACES_SPLAN_H

#include "Plan.h"
#include "MobileException.h"

namespace Company {

    class SPlan final : public Plan {
        float m_PricePerSecond;
    public:
        SPlan() : Plan("Good Plan or smth"), m_PricePerSecond(0.1) {}
        SPlan(std::string name, float price) : Plan(std::move(name)) { SetPrice(price, 0.1f); }

        bool SetPrice(float price, float fallback = 0.1f)
        {
            try {
                if(price <= 0)
                    throw MobileException("Price is negative! Setting it to " + std::to_string(fallback) + "\n");
                m_PricePerSecond = price;
                return true;
            }
            catch (MobileException& exception) {
                std::cout << exception.what();
                m_PricePerSecond = fallback;
                return false;
            }
        }

        float GetPrice() const {return m_PricePerSecond;}

        float CalculatePrice(int seconds) const override
        {
            try {
                if(seconds < 0)
                    throw MobileException("Seconds are negative! Setting them to 1\n");
                return m_PricePerSecond * float(seconds);
            }
            catch (MobileException& exception) {
                std::cout << exception.what();
                return m_PricePerSecond;
            }
        }
    };

} // Company

#endif //NAMESPACES_SPLAN_H
