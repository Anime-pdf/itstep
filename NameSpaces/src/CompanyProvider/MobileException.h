#ifndef NAMESPACES_MOBILEEXCEPTION_H
#define NAMESPACES_MOBILEEXCEPTION_H

#include <iostream>

namespace Company {

    class MobileException : public std::exception {
    public:
        MobileException() : m_Message("Unknown Exception\n") {}
        MobileException(std::string msg) : m_Message(std::move(msg)) {}
        std::string m_Message;

        virtual ~MobileException() = default;

        virtual const char* what() const noexcept {
            return m_Message.c_str();
        }
    };

} // Company

#endif //NAMESPACES_MOBILEEXCEPTION_H
