#ifndef NAMESPACES_USER_H
#define NAMESPACES_USER_H

#include <string>
#include <utility>
#include "Plan.h"

namespace Company {

    class User final {
        std::string m_FullName;
        std::string m_Number;
        float m_Balance;
        Plan* m_Plan;
    public:
        User() : m_FullName("John Doe"), m_Balance(0) {}
        User(std::string name, std::string phone, Plan* plan) : m_FullName(std::move(name)), m_Number(std::move(phone)) { m_Plan = plan; }
        ~User() {delete m_Plan;}
        std::string GetName() const {return m_FullName;}
        void SetName(std::string str) {m_FullName = std::move(str);}
        std::string GetPhone() const {return m_Number;}
        void SetPhone(std::string str) {m_Number = std::move(str);}
        Plan* GetPlan() const {return m_Plan;}
        void SetPlan(Plan* plan) {m_Plan = plan;}
        float GetBalance() const {return m_Balance;}
        void SetBalance(float b) {m_Balance = b;}
    };

} // Company

#endif //NAMESPACES_USER_H
