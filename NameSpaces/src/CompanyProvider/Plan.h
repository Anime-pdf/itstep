#ifndef NAMESPACES_PLAN_H
#define NAMESPACES_PLAN_H

#include <string>
#include <utility>
#include "MobileException.h"

namespace Company {

    class Plan {
    protected:
        std::string m_Name;
    public:
        Plan() : m_Name("Good stuff") {}
        Plan(std::string name) { SetName(std::move(name)); }

        std::string GetName() const {return m_Name;}
        void SetName(std::string str) {
            try{
                if(str.empty())
                    throw MobileException("Name in empty, setting it to default\n");
                m_Name = std::move(str);
            }
            catch (MobileException& exception) {
                std::cout << exception.what();
                m_Name = "Good stuff";
            }
        }
        virtual float CalculatePrice(int seconds) const = 0;
        virtual ~Plan() = default;
    };
} // Company

#endif //NAMESPACES_PLAN_H
