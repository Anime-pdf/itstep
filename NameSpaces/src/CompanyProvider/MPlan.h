//
// Created by paul on 26/12/22.
//

#ifndef NAMESPACES_MPLAN_H
#define NAMESPACES_MPLAN_H

#include <iostream>
#include "Plan.h"
#include "MobileException.h"

namespace Company {

    class MPlan final : public Plan {
        float m_PricePerMinute;
    public:
        MPlan() : Plan("Good Plan or smth"), m_PricePerMinute(0.6) {}
        MPlan(std::string name, float price) : Plan(std::move(name)) { SetPrice(price, 0.6); }

        bool SetPrice(float price, float fallback = 0.1f)
        {
            try {
                if(price <= 0)
                    throw MobileException("Price is negative! Setting it to " + std::to_string(fallback) + "\n");
                m_PricePerMinute = price;
                return true;
            }
            catch (MobileException& exception) {
                std::cout << exception.what();
                m_PricePerMinute = fallback;
                return false;
            }
        }

        float GetPrice() const {return m_PricePerMinute;}

        float CalculatePrice(int seconds) const override
        {
            try {
                if(seconds < 0)
                    throw MobileException("Seconds are negative! Setting them to 1\n");
                return m_PricePerMinute * float(seconds/60);
            }
            catch (MobileException& exception) {
                std::cout << exception.what();
                return m_PricePerMinute;
            }
        }
    };

} // Company

#endif //NAMESPACES_MPLAN_H
