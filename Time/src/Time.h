#ifndef TIME_H
#define TIME_H

#include <iostream>

using std::ostream;
using std::istream;

class Time
{
    int m_Hours;
    int m_Minutes;
    int m_Seconds;
    int m_TimeZone; // utc

    void nextSecond();
    void prevSecond();

public:
    enum Formats {
        TwentyFour = 0,
        Twelve,
    } m_Format;

    Time(); // Current local time
    Time(int hour, int minutes, int seconds, Formats format);
    Time(const Time& obj);
    Time(long long seconds, Formats format, int);
    Time& operator = (const Time& obj);

    bool setHour(int hour);
    int getHour()const;
    bool setMinutes(int minutes);
    int getMinutes()const;
    bool setSeconds(int seconds);
    int getSeconds()const;
    void setFormat(Formats format = TwentyFour); // !!! to update time by the new format: utc to pm
    Time::Formats getFormat()const; //utc/am/pm
    bool setTimeZone(int timezone);
    int getTimeZone()const;

    bool valid()const; //time check
    void tickTime(); //every tick add one second
    void untickTime(); //every tick remove one second
    void showTime()const; //show time by the format
    std::string returnFormatted()const;

    long long asMinutes() const { return m_Hours*60+m_Minutes; };
    long long asSeconds() const { return m_Hours*60*60+m_Minutes*60+m_Seconds; };

    //--------- Comparison operators ---------
    bool operator == (const Time& obj)const&;
    bool operator != (const Time& obj)const&;
    bool operator > (const Time& obj)const&;
    bool operator < (const Time& obj)const&;
    bool operator >= (const Time& obj)const&;
    bool operator <= (const Time& obj)const&;

    //--------- Assignment operators ---------
    Time& operator += (int n);
    Time& operator -= (int n);

    //--------- Arithmetic operators ---------
    Time operator + (int n)const&;
    Time operator - (int n)const&;

    Time operator - (const Time& r)const&;

    Time& operator -- ();
    Time operator -- (int n);
    Time& operator ++ ();
    Time operator ++ (int n);

    friend Time operator + (int n, Time a);
    friend Time operator - (int n, Time a);

    friend ostream& operator << (ostream& os, const Time& t);
    friend istream& operator >> (istream& is, Time& t);
};


#endif //TIME_H
