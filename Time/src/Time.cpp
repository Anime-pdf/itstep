#include <ctime>
#include <cstdio>
#include <sstream>
#include "Time.h"

void Time::nextSecond() {
    if(m_Seconds == 59)
    {
        if(m_Minutes == 59)
        {
            m_Hours++;
            m_Minutes = 0;
        }
        else
            m_Minutes++;
        m_Seconds = 0;
    }
    else
        m_Seconds++;
}

void Time::prevSecond() {
    if(m_Seconds == 0)
    {
        if(m_Minutes == 0)
        {
            m_Hours--;
            m_Minutes = 59;
        }
        else
            m_Minutes--;
        m_Seconds = 59;
    }
    else
        m_Seconds--;
}


Time::Time() {
    struct tm *tim = new tm;
    time_t tt = time(NULL);
    tim = localtime(&tt);
    setHour(tim->tm_hour);
    setMinutes(tim->tm_min);
    setSeconds(tim->tm_sec);
    setTimeZone(tim->tm_gmtoff/3600);
}

Time::Time(int hour, int minutes, int seconds, Formats format = TwentyFour) {
    if(!setHour(hour))
        setHour(12);
    if(!setMinutes(minutes))
        setMinutes(12);
    if(!setSeconds(seconds))
        setMinutes(12);
    setFormat(format);
}

Time::Time(const Time &obj) {
    setHour(obj.m_Hours);
    setMinutes(obj.m_Minutes);
    setSeconds(obj.m_Seconds);
    setFormat(obj.m_Format);
    setTimeZone(obj.m_TimeZone);
}

Time::Time(long long seconds, Formats format = TwentyFour, int tz = 0) {
    setHour(seconds/3600);
    setMinutes(seconds/60);
    setSeconds(seconds%60);
    setFormat(format);
    setTimeZone(tz);
}

Time &Time::operator=(const Time &obj) {
    setHour(obj.m_Hours);
    setMinutes(obj.m_Minutes);
    setSeconds(obj.m_Seconds);
    setFormat(obj.m_Format);
    setTimeZone(obj.m_TimeZone);
    return *this;
}

bool Time::setHour(int hour) {
    if (hour < 0 || hour > 23)
        return false;
    m_Hours = hour;
    return true;
}

int Time::getHour() const {
    return m_Hours;
}

bool Time::setMinutes(int minutes) {
    if (minutes < 0 || minutes > 59)
        return false;
    m_Minutes = minutes;
    return true;
}

int Time::getMinutes() const {
    return m_Minutes;
}

bool Time::setSeconds(int seconds) {
    if (seconds < 0 || seconds > 59)
        return false;
    m_Seconds = seconds;
    return true;
}

int Time::getSeconds() const {
    return m_Seconds;
}

void Time::setFormat(Time::Formats format) {
    m_Format = format;
}

Time::Formats Time::getFormat() const {
    return m_Format;
}

bool Time::setTimeZone(int time) {
    if(time < -12 || time > 14)
        return false;
    m_TimeZone = time;
    return true;
}

int Time::getTimeZone() const {
    return m_TimeZone;
}

bool Time::valid() const {
    if (m_Hours < 0 || m_Hours > 23)
        return false;
    if (m_Minutes < 0 || m_Minutes > 59)
        return false;
    if (m_Seconds < 0 || m_Seconds > 59)
        return false;
    if(m_TimeZone < -12 || m_TimeZone > 14)
        return false;
}

void Time::tickTime() {
    nextSecond();
}

void Time::untickTime() {
    prevSecond();
}

void Time::showTime() const {
    if (m_Format == Twelve)
        printf("%d:%d:%d %s UTC%s%d", m_Hours>11?m_Hours-12:m_Hours, m_Minutes, m_Seconds, m_Hours>11?"pm":"am", m_TimeZone>=0?"+":"", m_TimeZone);
    else
        printf("%d:%d:%d UTC%s%d", m_Hours, m_Minutes, m_Seconds, m_TimeZone>=0?"+":"", m_TimeZone);
}

std::string Time::returnFormatted() const {
    char buf[100];
    if (m_Format == Twelve)
        snprintf(buf, sizeof(buf), "%d:%d:%d %s UTC%s%d", m_Hours > 11 ? m_Hours - 12 : m_Hours, m_Minutes, m_Seconds, m_Hours > 11 ? "pm" : "am", m_TimeZone >= 0 ? "+" : "", m_TimeZone);
    else
        snprintf(buf, sizeof(buf), "%d:%d:%d UTC%s%d", m_Hours, m_Minutes, m_Seconds, m_TimeZone >= 0 ? "+" : "", m_TimeZone);
    return {buf};
}

bool Time::operator==(const Time &obj) const &{
    if(obj.getTimeZone() != getTimeZone())
        return false;
    if(obj.getHour() != getHour())
        return false;
    if(obj.getMinutes() != getMinutes())
        return false;
    if(obj.getSeconds() != getSeconds())
        return false;
    return true;
}

bool Time::operator!=(const Time &obj) const &{
    if(obj.getTimeZone() != getTimeZone())
        return true;
    if(obj.getHour() != getHour())
        return true;
    if(obj.getMinutes() != getMinutes())
        return true;
    if(obj.getSeconds() != getSeconds())
        return true;
    return false;
}

bool Time::operator>(const Time &obj) const &{
    if(getHour()+getTimeZone() > obj.getHour()+obj.getTimeZone())
        return true;
    else if(getHour()+getTimeZone() == obj.getHour()+obj.getTimeZone())
        if(getMinutes() > obj.getMinutes())
            return true;
        else if(getMinutes() == obj.getMinutes())
            if(getSeconds() > obj.getSeconds())
                return true;
    return false;
}

bool Time::operator<(const Time &obj) const &{
    if(getHour()+getTimeZone() < obj.getHour()+obj.getTimeZone())
        return true;
    else if(getHour()+getTimeZone() == obj.getHour()+obj.getTimeZone())
        if(getMinutes() < obj.getMinutes())
            return true;
        else if(getMinutes() == obj.getMinutes())
            if(getSeconds() < obj.getSeconds())
                return true;
    return false;
}

bool Time::operator>=(const Time &obj) const &{
    if(getHour()+getTimeZone() > obj.getHour()+obj.getTimeZone())
        return true;
    else if(getHour()+getTimeZone() == obj.getHour()+obj.getTimeZone())
        if(getMinutes() > obj.getMinutes())
            return true;
        else if(getMinutes() == obj.getMinutes())
            if(getSeconds() >= obj.getSeconds())
                return true;
    return false;
}

bool Time::operator<=(const Time &obj) const &{
    if(getHour()+getTimeZone() < obj.getHour()+obj.getTimeZone())
        return true;
    else if(getHour()+getTimeZone() == obj.getHour()+obj.getTimeZone())
        if(getMinutes() < obj.getMinutes())
            return true;
        else if(getMinutes() == obj.getMinutes())
            if(getSeconds() <= obj.getSeconds())
                return true;
    return false;
}

Time &Time::operator+=(int n) {
    for(int i = 0; i < n; i++)
        nextSecond();
    return *this;
}

Time &Time::operator-=(int n) {
    for(int i = 0; i < n; i++)
        prevSecond();
    return *this;
}

Time Time::operator+(int n) const &{
    Time tmp = *this;
    for(int i = 0; i < n; i++)
        tmp.nextSecond();
    return tmp;
}

Time Time::operator-(int n) const &{
    Time tmp = *this;
    for(int i = 0; i < n; i++)
        tmp.prevSecond();
    return tmp;
}

Time &Time::operator--() {
    prevSecond();
    return *this;
}

Time Time::operator--(int n) {
    Time tmp = *this;
    prevSecond();
    return tmp;
}

Time &Time::operator++() {
    nextSecond();
    return *this;
}

Time Time::operator++(int n) {
    Time tmp = *this;
    nextSecond();
    return tmp;
}

Time operator+(int n, Time a) {
    Time tmp = a;
    for(int i = 0; i < n; i++)
        tmp.nextSecond();
    return tmp;
}

Time operator-(int n, Time a) {
    Time tmp = a;
    for(int i = 0; i < n; i++)
        tmp.prevSecond();
    return tmp;
}

ostream &operator<<(ostream &os, const Time &t) {
    t.showTime();
    return os;
}

istream &operator>>(istream &is, Time &t) {
    int Hours;
    int Minutes;
    int Seconds;
    int TimeZone; // utc
    do {
        printf("Enter Hours: ");
        is >> Hours;
    } while (!t.setHour(Hours));
    do {
        printf("Enter Minutes: ");
        is >> Hours;
    } while (!t.setMinutes(Minutes));
    do {
        printf("Enter Seconds: ");
        is >> Hours;
    } while (!t.setSeconds(Seconds));
    do {
        printf("Enter TimeZone: ");
        is >> TimeZone;
    } while (!t.setTimeZone(Seconds));
    t.setFormat(Time::TwentyFour);
    return is;
}

Time Time::operator-(const Time& r) const &{
    return {asSeconds() - r.asSeconds(), getFormat(), getTimeZone()};
}
