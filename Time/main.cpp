#include <iostream>
#include "src/Time.h"

int main() {
    Time_ t;
    t.setFormat(Time_::Twelve);
    t.showTime();
    return 0;
}
