#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

using std::string;
using std::cin;
using std::cout;
using std::getline;
int main() {
//    //1
//    {
//        int n;
//        string s;
//        cin >> n;
//        getline(cin, s);
//        if (s.length() > n)
//            s.erase(0, s.length() - n);
//        else if (s.length() < n) {
//            string string1(n - s.length(), '.');
//            string1.append(s);
//            s = string1;
//        }
//
//        cout << s;
//    }
//    //2
//    {
//        int n1,n2;
//        string s1,s2;
//        cin >> n1 >> n2;
//        getline(cin, s1);
//        getline(cin, s2);
//
//        string out = s1.substr(0, n1) + s2.substr(s2.length()-n2, n2);
//        cout << out;
//    }
//    //3
//    {
//        char c;
//        string s;
//        cin >> c;
//        cin.ignore();
//        getline(cin, s);
//        string from(1, c), to(2, c);
//        size_t pos = s.find(from, 0);
//        while (pos != string::npos) {
//            s.replace(pos, 1, to);
//            pos = s.find(from, pos + 2);
//        }
//        cout << s;
//    }
//    //4
//    {
//        char c;
//        string s, s0;
//        cin >> c;
//        cin.ignore();
//        getline(cin, s);
//        getline(cin, s0);
//        string from(1, c), to(s0); to.append(from);
//        size_t pos = s.find(from, 0);
//        while (pos != string::npos) {
//            s.replace(pos, 1, to);
//            pos = s.find(from, pos + to.length()+1);
//        }
//        cout << s;
//    }
//    //5
//    {
//        char c;
//        string s, s0;
//        cin >> c;
//        cin.ignore();
//        getline(cin, s);
//        getline(cin, s0);
//        string from(1, c), to(from); to.append(s0);
//        size_t pos = s.find(from, 0);
//        while (pos != string::npos) {
//            s.replace(pos, 1, to);
//            pos = s.find(from, pos + to.length()+1);
//        }
//        cout << s;
//    }
//    //6
//    {
//        string s, s0;
//        cin.ignore();
//        getline(cin, s);
//        getline(cin, s0);
//        cout << (s.find(s0) != string::npos) << std::endl;
//    }
//    //7
//    {
//        string s, s0;
//        getline(cin, s);
//        getline(cin, s0);
//        size_t pos = 0;
//        int matches = 0;
//        do {
//            pos = s.find(s0, pos);
//            if(pos == string::npos)
//                break;
//            matches++;
//            pos++;
//        } while (true);
//        cout << matches;
//    }
//    //8
//    {
//        string s, s0;
//        getline(cin, s);
//        getline(cin, s0);
//        size_t pos = s.find(s0, 0);
//        if(pos != string::npos)
//            s.erase(pos, s0.length());
//        cout << s;
//    }
//    //9
//    {
//        string s, s0;
//        getline(cin, s);
//        getline(cin, s0);
//        size_t pos = s.find(s0, 0);
//        while (pos != string::npos) {
//            s.erase(pos, s0.length());
//            pos = s.find(s0, 0);
//        }
//        cout << s;
//    }
//    //10
//    {
//        string s, s1, s2;
//        getline(cin, s);
//        getline(cin, s1);
//        getline(cin, s2);
//        size_t pos = s.find(s1, 0);
//        if(pos != string::npos)
//            s.replace(pos, s1.length(), s2);
//        cout << s;
//    }
//    //11
//    {
//        string s, s1, s2;
//        getline(cin, s);
//        getline(cin, s1);
//        getline(cin, s2);
//        size_t pos = s.find(s1, 0);
//        while (pos != string::npos) {
//            s.replace(pos, s1.length(), s2);
//            pos = s.find(s1, 0);
//        }
//        cout << s;
//    }
//    //12
//    {
//        string s, word;
//        size_t g_pos = 0;
//        getline(cin, s);
//        std::istringstream ss(s);
//        while (ss >> word)
//        {
//            g_pos = s.find(word, g_pos);
//            string last(1, word[word.length()-1]);
//            size_t pos = word.find(last, 0);
//            while (pos != string::npos && pos != word.length()-1) {
//                word.replace(pos, 1, ".");
//                pos = word.find(last, 0);
//            }
//            s.replace(g_pos, word.length(), word);
//            g_pos++;
//        }
//        cout << s;
//    }
//    //13
//    {
//        string s,s2;
//        getline(cin, s);
//        for (int i = 0; i < s.length(); i+=2)
//            s2+=s[i];
//        for (int i = 1; i < s.length(); i += 2)
//            s2.insert(s2.length() - (i - 1) / 2, string(1, s[i]));
//        cout << s2;
//    }
//    //14
//    { // Pormamagr
//        string s,s2;
//        getline(cin, s);
//        cout << s.length() << std::endl;
//        for (int i = 0,j=0; i < s.length(); ++i) {
//            if (!(i % 2))
//                s2 += s[i / 2];
//            else {
//                s2 += s[s.length() - i + j];
//                j++;
//            }
//        }
//        cout << s2;
//    }

//    https://tenor.com/biYfD.gif

    return 0;
}
