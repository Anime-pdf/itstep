#ifndef LESSONS_PAIDLESSON_H
#define LESSONS_PAIDLESSON_H

#include "Lesson.h"

class PaidLesson : Lesson {
    float price_;
public:
    PaidLesson(float price = 10,
               std::string title = "Inheritance C++",
               std::string desc = "Mastering oop",
               std::string hw = "Write smth")
               : Lesson(title, desc, hw) {
        setPrice(price);
    }

    bool setPrice(float price) {
        if(price < 0)
            return false;
        price_ = price;
        return true;
    }

    void print() {
        printf("Title: %s\n"
               "Description: %s\n"
               "Homework: %s\n"
               "Price: %.2f\n",
               title_.c_str(),
               desc_.c_str(),
               homework_.c_str(),
               price_);
    }
};

#endif //LESSONS_PAIDLESSON_H
