#ifndef LESSONS_LESSON_H
#define LESSONS_LESSON_H

#include <string>

class Lesson {
protected:
    std::string title_;
    std::string desc_;
    std::string homework_;
public:
    Lesson(std::string title = "Inheritance C++", std::string desc = "Mastering oop", std::string hw = "Write smth") {
        setTitle(title);
        setDesc(desc);
        setHomework(hw);
    }

    std::string getTitle() { return title_; }
    std::string getDescription() { return desc_; }
    std::string getHomework() { return homework_; }

    void setTitle(std::string title) { title_ = title; }
    void setDesc(std::string desc) { desc_ = desc; }
    void setHomework(std::string homework) { homework_ = homework; }

    void print() {
        printf("Title: %s\nDescription: %s\nHomework: %s\n", title_.c_str(), desc_.c_str(), homework_.c_str());
    }
};

#endif //LESSONS_LESSON_H
