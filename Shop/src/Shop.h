#ifndef SHOP_SHOP_H
#define SHOP_SHOP_H

#include "Product.h"

class Shop {
    int m_AmountOfProduct;
    Product* m_Products;
public:
    Shop();
    ~Shop();

    void addProduct(Product p);
    void delProduct(Product p);
    float delExpiredProducts(); //returns total lost profit

    friend ostream& operator << (ostream& out, const Shop& obj);
};

#endif //SHOP_SHOP_H
