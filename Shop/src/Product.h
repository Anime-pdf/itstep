#ifndef SHOP_PRODUCT_H
#define SHOP_PRODUCT_H

#include <src/Date.h>

class Product
{
    char *m_Title;
    float m_Price;
    Date m_Date;
    int m_DaysSave;

public:
    Product();
    Product(const char *title, float price, Date date, int daysSave);
    Product(const Product& obj);
    ~Product();

    void setTitle(const char *title);
    const char * getTitle()const;
    bool setPrice(float price);
    float getPrice()const;
    int getDaysSave()const;

    bool isExpired();

    bool operator==(const Product& obj);

    Product& operator=(const Product& obj);
    friend ostream& operator << (ostream& out, const Product& obj);
    friend istream& operator >> (istream& in, Product& obj);
};

#endif //SHOP_PRODUCT_H
