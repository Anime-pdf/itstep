//
// Created by paul on 14/10/22.
//

#include "Shop.h"

Shop::Shop() : m_AmountOfProduct(0), m_Products(nullptr) {}

Shop::~Shop() {
    delete[] m_Products;
}

void Shop::addProduct(Product p) {
    Product* tmp = new Product[m_AmountOfProduct+1];
    for (int i = 0; i < m_AmountOfProduct; ++i)
        tmp[i] = m_Products[i];
    tmp[m_AmountOfProduct++] = p;
    m_Products = tmp;
}

void Shop::delProduct(Product p) {
    int kek=0;
    for (int i = 0; i < m_AmountOfProduct; ++i) {
        if (m_Products[i] == p)
            kek++;
    }
    Product* tmp = new Product[m_AmountOfProduct-kek];
    for (int i = 0, j=0; i < m_AmountOfProduct; ++i) {
        if (m_Products[i] == p)
            continue;
        tmp[j] = m_Products[i];
        j++;
    }
    m_Products = tmp;
    m_AmountOfProduct-=kek;
}

float Shop::delExpiredProducts() {
    int kek=0;
    float loss=0;
    for (int i = 0; i < m_AmountOfProduct; ++i) {
        if (m_Products[i].isExpired())
        {
            kek++;
            loss+=m_Products[i].getPrice();
        }
    }
    Product* tmp = new Product[m_AmountOfProduct-kek];
    for (int i = 0, j=0; i < m_AmountOfProduct; ++i) {
        if (m_Products[i].isExpired())
            continue;
        tmp[j] = m_Products[i];
        j++;
    }
    m_Products = tmp;
    m_AmountOfProduct-=kek;
    return loss;
}

ostream &operator<<(ostream &out, const Shop &obj) {
    out << "Products: " << std::endl;
    for (int i = 0; i < obj.m_AmountOfProduct; ++i)
        out << obj.m_Products[i] << std::endl;
    return out;
}
