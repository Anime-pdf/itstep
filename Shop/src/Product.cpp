#include <cstring>
#include "Product.h"

Product::Product() : m_Date(), m_DaysSave(14), m_Price(0.99f) {
    setTitle("Air");
}

Product::Product(const char *title, float price, Date date, int daysSave) {
    setTitle(title);
    setPrice(price);
    m_Date = date;
    m_DaysSave = m_DaysSave < 0 ? 14 : daysSave;
}

Product::Product(const Product &obj) {
    setTitle(obj.getTitle());
    setPrice(obj.getPrice());
    m_Date = obj.m_Date;
    m_DaysSave = obj.getDaysSave();
}

Product::~Product() {
    delete[] m_Title;
}

void Product::setTitle(const char *title) {
    m_Title = new char[strlen(title)+1];
    strcpy(m_Title, title);
}

const char *Product::getTitle() const {
    return m_Title;
}

bool Product::setPrice(float price) {
    if (price < 0)
        return false;
    m_Price = price;
    return true;
}

float Product::getPrice() const {
    return m_Price;
}

int Product::getDaysSave() const {
    return m_DaysSave;
}

bool Product::isExpired() {
    return (m_Date - m_DaysSave) < Date(); // default constructor returns current date
}

Product &Product::operator=(const Product &obj) {
    setTitle(obj.getTitle());
    setPrice(obj.getPrice());
    m_Date = obj.m_Date;
    m_DaysSave = obj.getDaysSave();
    return *this;
}

ostream &operator<<(ostream &out, const Product &obj) {
    out << "Title: " << obj.getTitle() << std::endl;
    out << "Price: " << obj.getPrice();
    if(obj.m_Date + obj.getDaysSave() - Date() <= 7)
        out << " -25% = " << obj.getPrice() - (25/obj.getPrice())*100;
    out << std::endl;
    out << "Packed: " << obj.m_Date << std::endl;
    out << "Valid until: " << obj.m_Date + obj.getDaysSave();
    return out;
}

istream &operator>>(istream &in, Product &obj) {
    std::string title;
    float price;
    int daysSafe;

    printf("Enter Title: ");
    in >> title;
    obj.setTitle(title.c_str());
    do {
        printf("Enter Price: ");
        in >> price;
    } while (!obj.setPrice(price));
    do {
        printf("Enter Expire Days: ");
        in >> daysSafe;
    } while (daysSafe <= 0);
    printf("Enter Date: ");
    in >> obj.m_Date;
    return in;
}

bool Product::operator==(const Product &obj) {
    if(strcmp(obj.getTitle(), getTitle()) || obj.getPrice() != getPrice())
        return false;
    return true;
}
