#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

template<class T>
void print (std::vector<T> v)
{
    for (auto &x: v) {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}

int main() {
    srand(time(nullptr));
    {
        std::vector<int> a;
        for (int i = 0; i < 50; ++i) {
            a.push_back(rand() % 100 - 50);
        }

        //1
        a.insert(a.begin() + a.size() / 2, 5, 0);
        print(a);
        //2
        a.erase(a.begin() + a.size() / 2 - 2, a.begin() + a.size() / 2 + 1);
        print(a);
        //3
        std::cout << *(std::max_element(a.begin(), a.end())) << std::endl;
        //4
        if (*(std::find(std::find(a.begin(), a.end(), 0) + 1, a.end(), 0)) ==
            0)// || (std::find(std::find(a.begin(), a.end(), 0)+1, a.end(), 0) == a.end() && *(a.end()) == 0))
            a.erase(std::find(std::find(a.begin(), a.end(), 0) + 1, a.end(), 0));
        print(a);
    }
    //5
    {
        std::vector<int> v1,v2;
        int a = 100,b = 200;
        for (int i = 0; i < 11; ++i) {
            v1.push_back(rand() % 100 - 50);
            v2.push_back(rand() % 100 - 50);
        }
        print(v1);
        print(v2);

        std::fill(v1.begin(), v1.begin()+v1.size()/2, a);
        std::fill(v1.begin()+v1.size()/2, v1.end(), b);

        std::fill_n(v2.begin(), v2.size()/2, a);
        std::fill_n(v2.begin()+v1.size()/2, ceil(v2.size()/2.f), b);
        print(v1);
        print(v2);
    }
    //6
    {
        std::vector<int> v;
        for (int i = 0; i < 10; ++i) {
            v.push_back(rand() % 100 - 50);
        }
        print(v);
        std::replace_if(v.begin(), v.begin()+v.size()/2, [](int a){if(a<0)return true;return false;}, -1);
        std::replace_if(v.begin()+v.size()/2, v.end(), [](int a){if(a>0)return true;return false;}, 1);
        print(v);
    }


    return 0;
}
