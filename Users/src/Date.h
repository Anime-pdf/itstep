#ifndef DATE_H
#define DATE_H

#include <cstdio>
#include <ctime>

class Date
{
    int m_Day;
    int m_Month;
    int m_Year;
public:
    bool isEqualToTm(std::tm* time)
    {
        if (time->tm_year != m_Year)
            return false;
        if (time->tm_mon != m_Month)
            return false;
        if (time->tm_mday != m_Day)
            return false;
        return true;
    }

    Date(int day = 1, int month = 1, int year = 2022)
    {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    void setYear(int year){
        if(year<0)
            return;
        m_Year = year;
    }
    void setMonth(int month){
        if(month<0 || month>12)
            return;
        m_Month = month;
    }
    void setDay(int day){
        if(day<0 || day>31)
            return;
        m_Day = day;
    }

    int getYear()const{return m_Year;}
    int getMonth()const{return m_Month;}
    int getDay()const{return m_Day;}

    void showDate()const{printf("%d/%d/%d\n",m_Day,m_Month,m_Year);}
};

#endif //DATE_H
