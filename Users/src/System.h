#ifndef SYSTEM_H
#define SYSTEM_H

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include "User.h"

class System {
    User* findUserByUsername(const char* username)
    {
        FILE *file;
        User temp;
        if((file = fopen(username,"r"))!=nullptr)
        {
            temp.fromBinFile(file);
            fclose(file);
            return &temp;
        }
        else
            return nullptr;
    }
    struct stat getFileCreationTime(char *path) {
        struct stat attr;
        stat(path, &attr);
        return attr;
    }
    void printUser(User* usr)
    {
        printf("\nUsername: %s\nEmail: %s\nBirthday date: ", usr->getLogin(), usr->getBirthDate());
        usr->getBirthDate().showDate();
    }
public:
    void searchAndPrint(const char* username)
    {
        User* tmp = findUserByUsername(username);
        if(tmp)
            printUser(tmp);
        else
            printf("\nUser not found!\n");
    }
    void searchAndPrintLostUsers()
    {
        User* tmp;
        DIR *d;
        FILE* fp;
        struct dirent *dir;
        d = opendir(".");
        if (d) {
            while ((dir = readdir(d)) != NULL) {
                std::time_t t = std::time(0);
                struct stat a = getFileCreationTime(dir->d_name);
                if(std::difftime(t, a.st_mtime) >= 2592000) //30 days
                {
                    fp = fopen(dir->d_name,"r");
                    tmp->fromBinFile(fp);
                    fclose(fp);
                    printUser(tmp);
                }
            }
            closedir(d);
        }
    }
    void banUser(const char* username)
    {
        User* tmp = findUserByUsername(username);
        if(tmp)
            remove(username);
        else
            printf("\nUser not found!\n");
    }
};


#endif //SYSTEM_H
