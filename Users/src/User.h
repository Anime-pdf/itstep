#ifndef USER_H
#define USER_H

#include <cstring>
#include <cctype>
#include <cstdlib>
#include "Date.h"

bool isChar(char c)
{
    return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

class User
{
    char* m_Login;
    char* m_Password;
    char* m_Email;
    Date m_Birth;

    bool isLoginAvail(const char* login) const {
        if (!isChar(login[0]) || strlen(login) < 4)
            return false;
        FILE *file;
        if((file = fopen(login,"r"))!=nullptr)
            fclose(file);
        else
            return false;
        return true;
    }
    bool isValidEmail(const char* email) const {
        if (!isChar(email[0]))
            return false;
        int at = -1, host = -1;
        for (int i = 0; i < strlen(email); i++)
        {
            if (email[i] == '@')
                at = i;
            else if (email[i] == '.')
                host = i;
        }
        if (at == -1 || host == -1)
            return false;
        if (at > host)
            return false;
        if(host >= strlen(email) - 1)
            return false;
        return true;
    }
    int calcComplexity(const char* password) const {
        int n = strlen(password);
        bool hasLower = false, hasUpper = false, hasDigit = false, hasSpecial = false;
        const char specials[] = "@%+\\/'!#$^?:,(){}[]~._-*";
        for (int i = 0; i < n; i++) {
            if (islower(password[i]))
                hasLower = true;
            if (isupper(password[i]))
                hasUpper = true;
            if (isdigit(password[i]))
                hasDigit = true;
        }
        for(int j = 0; j < strlen(specials); j++)
                if(strchr(password, specials[j]) != nullptr) {
                    hasSpecial = true;
                    break;
                }

        if (hasLower && hasUpper && hasDigit && hasSpecial && (n >= 8))
             return 2;
        else if ((hasLower || hasUpper) &&
                 hasSpecial && (n >= 6))
            return 1;
        else
            return 0;
    }
public:
    User(const char* login = "NewUser", const char* password = "qwerty", const char* email = nullptr){
        setLogin(login);
        setPassword(password);
        setEmail(email);
    }
    ~User(){
        delete[] m_Login;
        delete[] m_Password;
        delete[] m_Email;
    }

    bool isTodayBithday()
    {
        std::time_t t = std::time(0);
        std::tm* now = std::localtime(&t);
        return m_Birth.isEqualToTm(now);
    }

    void setLogin(const char* login) {
        if (!isLoginAvail(login))
            return;
        m_Login = new char[strlen(login)+1];
        strcpy(m_Login, login);
    }
    void setPassword(const char* password) {
        if (calcComplexity(password) <= 0)
            return;
        m_Password = new char[strlen(password)+1];
        strcpy(m_Password, password);
    }
    void setEmail(const char* email) {
        if(!isValidEmail(email))
            return;
        if(email == nullptr)
        {
            m_Email = nullptr;
            return;
        }
        m_Email = new char[strlen(email)+1];
        strcpy(m_Email, email);
    }
    void setDate(Date date) {m_Birth = date;}

    const char* getLogin()const{return m_Login;}
    const char* getPassword()const{return m_Password;}
    const char* getEmail()const{return m_Email;}
    Date getBirthDate()const{return m_Birth;}

    void toBinFile(FILE* fp){
        if(!fp)
            return;
        int size;

        size = strlen(m_Login);
        fwrite(&size, sizeof(int), 1, fp);
        fwrite(m_Login, sizeof(char), size, fp);
        size = strlen(m_Password);
        fwrite(&size, sizeof(int), 1, fp);
        fwrite(m_Password, sizeof(char), size, fp);
        if(m_Email== nullptr) {
            size = -1;
            fwrite(&size, sizeof(int), 1, fp);
        }
        else
        {
            size = strlen(m_Email);
            fwrite(&size, sizeof(int), 1, fp);
            fwrite(m_Email, sizeof(char), size, fp);
        }
        fwrite(&m_Birth, sizeof(Date), 1, fp);
    }
    void fromBinFile(FILE* fp){
        if(!fp)
            return;
        int size;

        fread(&size, sizeof(int), 1, fp);
        m_Login = new char[size+1];
        fread(m_Login, sizeof(char), size, fp);
        m_Login[size] = '\0';

        fread(&size, sizeof(int), 1, fp);
        m_Password = new char[size+1];
        fread(m_Password, sizeof(char), size, fp);
        m_Password[size] = '\0';

        fread(&size, sizeof(int), 1, fp);
        if(size == -1)
            m_Email = nullptr;
        else {
            m_Email = new char[size+1];
            fread(m_Email, sizeof(char), size, fp);
            m_Email[size] = '\0';
        }
        fread(&m_Birth, sizeof(Date), 1, fp);
    }

    void codePassword() {
        char ch;
        int key = m_Birth.getMonth();
        for(int i = 0; m_Password[i] != strlen(m_Password); ++i) {
            ch = m_Password[i];
            if (islower(ch)){
                ch += key;
                if (ch > 'z')
                    ch = ch - 'z' + 'a' - 1;
                m_Password[i] = ch;
            }
            else if (isupper(ch)){
                ch += key;
                if (ch > 'Z')
                    ch -= 'Z' + 'A' - 1;
                m_Password[i] = ch;
            }
        }
    }
    void decodePassword() {
        char ch;
        int key = m_Birth.getMonth();
        for(int i = 0; m_Password[i] != strlen(m_Password); ++i) {
            ch = m_Password[i];
            if(islower(ch)) {
                ch -= key;
                if(ch < 'a')
                    ch += 'z' - 'a' + 1;
                m_Password[i] = ch;
            }
            else if(isupper(ch)) {
                ch -= key;
                if(ch < 'A')
                    ch += 'Z' - 'A' + 1;
                m_Password[i] = ch;
            }
        }
    }

    void signUp() {
        FILE* fp = fopen(m_Login, "w");
        toBinFile(fp);
        fclose(fp);
    }
    bool signIn(const char* login, const char* password) {
        FILE* fp;
        if((fp = fopen(login,"r"))!=nullptr) {
            fromBinFile(fp);
            fclose(fp);
            if (!strcmp(m_Password, password)) {
                if(isTodayBithday())
                {
                    printf("Happy Birthday!");
                    system("start 1.jpg");
                }
                return true;
            }
        }
        return false;
    }
};

#endif //USER_H
