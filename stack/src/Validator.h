#ifndef STACK_VALIDATOR_H
#define STACK_VALIDATOR_H

#include <string>
#include <stack>

class Validator {
    static inline bool isOpening(const char c)
    {
        return (c == '{' || c == '(' || c == '[');
    }
    static inline bool isClosing(const char c)
    {
        return (c == '}' || c == ')' || c == ']');
    }
    static inline bool isCouple(const char open, const char close)
    {
        return ((open == '{' && close == '}') || (open == '(' && close == ')') || (open == '[' && close == ']'));
    }
public:
    static bool Validate(const std::string& str)
    {
        std::stack<char> v;
        for (const auto &item: str)
        {
            if(!isOpening(item) && !isClosing(item))
                continue;

            if(isOpening(item))
                v.push(item);

            if(isClosing(item))
            {
                if(isCouple(v.top(), item))
                    v.pop();
                else
                    return false;
            }
        }
        return v.empty();
    }
};

#endif //STACK_VALIDATOR_H
