#ifndef STACK_MULTICOOK_H
#define STACK_MULTICOOK_H

#include <string>
#include <utility>
#include <queue>
#include <iostream>

class MultiCook {
public:
    class Ingredient
    {
        std::string name_;
        int cooking_time_;
    public:
        Ingredient(std::string name, int cooking_time)
        {
            if((name_ = std::move(name)).empty())
                name_ = "Potato";
            if((cooking_time_ = cooking_time) < 1)
                cooking_time_ = 1;

        }

        int getTime() const {return cooking_time_;}
        std::string getName() const {return name_;}

        friend bool operator < (const Ingredient& a, const Ingredient& b) {
            return a.getTime() > b.getTime();
        }
    };
private:

    std::priority_queue<Ingredient> ingredients_;
public:
    void addIngredient(const Ingredient& ingredient)
    {
        ingredients_.push(ingredient);
    }

    void cook()
    {
        while (!ingredients_.empty()) {
            std::cout << ingredients_.top().getName() << " - cooked in " << ingredients_.top().getTime() << std::endl;
            ingredients_.pop();
        }
        std::cout << "Dish is cooked!";
    }

};

#endif //STACK_MULTICOOK_H
