#include <iostream>
#include "src/Validator.h"
#include "src/MultiCook.h"

int main() {
    std::string valid = "asd[dsaasd]dsa[s{s}()]";
    std::string invalid = "asd[dsa(asd]dsa[s{)s}()]";
    std::cout << valid << " - " << Validator::Validate(valid) << std::endl;
    std::cout << invalid << " - " << Validator::Validate(invalid) << std::endl;

    std::cout << std::endl;

    MultiCook cook;
    cook.addIngredient(MultiCook::Ingredient("Potato", 1200));
    cook.addIngredient(MultiCook::Ingredient("Onion", 160));
    cook.addIngredient(MultiCook::Ingredient("Bacon", 360));
    cook.cook();
    return 0;
}
