//
// Created by paul on 28/10/22.
//

#ifndef WIRELESSADAPTER_ADAPTER_H
#define WIRELESSADAPTER_ADAPTER_H

#include <string>
#include <iostream>
#include <fstream>

using std::string;
using std::to_string;

class Adapter {
    string m_ID;
    string m_Type;
    float m_Price;
    float m_Speed;

    static inline int Total{0};
public:
    Adapter(string type, float price, float speed) : m_ID(string("WFID"+ to_string(++Total))), m_Type(type), m_Price(price), m_Speed(speed) {}

    string getId()const {return m_ID;}
    string getType()const {return m_Type;}
    float getPrice()const {return m_Price;}
    float getSpeed()const {return m_Speed;}
    static int getTotal() {return Total;}

    void printInfo()
    {
        printf("Id: %s\nType: %s\nPrice: %.2f\nSpeed: %.2f mbit/s\n", m_ID.c_str(), m_Type.c_str(), m_Price, m_Speed);
    }

    void saveToFile(std::ofstream& fout){
        fout << m_ID << std::endl;
        fout << m_Type << std::endl;
        fout << m_Speed << " " << m_Price << std::endl;
    }
    void loadFromFile(std::ifstream& fin){
        fin >> m_ID;
        fin.ignore();
        getline(fin, m_Type);
        fin >> m_Speed >> m_Price;
        if(m_ID.size() == 7)
        {
            int id = atoi(m_ID.substr(4).c_str());
            Total = id > Total ? id : Total;
        }
    }
};


#endif //WIRELESSADAPTER_ADAPTER_H
