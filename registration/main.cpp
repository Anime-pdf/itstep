#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "AccountsManager.h"

std::vector<std::string> tokenize(std::string const &str, const char delim)
{
    std::vector<std::string> out;
    std::stringstream ss(str);
    std::string s;
    while (std::getline(ss, s, delim)) {
        out.push_back(s);
    }
    return out;
}

int main() {
    AccountsManager system;

    std::string input;
    do {
        std::getline(std::cin, input);
        auto args = tokenize(input, ' ');

        if (args.at(0) == "register") {
            switch (system.Register(args.at(1), args.at(2))) {
                case AccountsManager::REGISTER_STATUS_OK:
                    std::cout << "success: new user added\n";
                    break;
                case AccountsManager::REGISTER_STATUS_ALREADY_REGISTERED:
                    std::cout << "fail: user already exists\n";
                    break;
            }
        }
        if (args.at(0) == "login") {
            switch (system.Login(args.at(1), args.at(2))) {
                case AccountsManager::LOGIN_STATUS_OK:
                    std::cout << "success: user logged in\n";
                    break;
                case AccountsManager::LOGIN_STATUS_ALREADY_LOGGED:
                    std::cout << "fail: already logged in\n";
                    break;
                case AccountsManager::LOGIN_STATUS_INCORRECT_PASS:
                    std::cout << "fail: incorrect password\n";
                    break;
                case AccountsManager::LOGIN_STATUS_NO_USER:
                    std::cout << "fail: no such user\n";
                    break;
            }
        }
        if (args.at(0) == "logout") {
            switch (system.Logout(args.at(1))) {
                case AccountsManager::LOGOUT_STATUS_OK:
                    std::cout << "success: user logged out\n";
                    break;
                case AccountsManager::LOGOUT_STATUS_ALREADY_LOGGED_OUT:
                    std::cout << "fail: already logged out\n";
                    break;
                case AccountsManager::LOGOUT_STATUS_NO_USER:
                    std::cout << "fail: no such user\n";
                    break;
            }
        }
    }
    while (input != "exit");
}
