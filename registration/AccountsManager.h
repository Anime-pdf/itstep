#ifndef REGISTRATION_ACCOUNTSMANAGER_H
#define REGISTRATION_ACCOUNTSMANAGER_H

#include <map>
#include <string>
#include <vector>

class AccountsManager {
    std::map<std::string /* login */, std::string /* password */> Users;
    std::vector<std::string /*login */> LoggedInUsers;

    bool UserExists(const std::string& login) { return Users.find(login) != Users.end(); }
    bool UserLoggedIn(const std::string& login) { return std::find(LoggedInUsers.begin(), LoggedInUsers.end(), login) != LoggedInUsers.end(); }
public:
    enum STATUS_CODE_LOGIN {
        LOGIN_STATUS_OK = 0,
        LOGIN_STATUS_NO_USER,
        LOGIN_STATUS_INCORRECT_PASS,
        LOGIN_STATUS_ALREADY_LOGGED,
    };
    enum STATUS_CODE_REGISTER {
        REGISTER_STATUS_OK = 0,
        REGISTER_STATUS_ALREADY_REGISTERED,
    };
    enum STATUS_CODE_LOGOUT {
        LOGOUT_STATUS_OK = 0,
        LOGOUT_STATUS_NO_USER,
        LOGOUT_STATUS_ALREADY_LOGGED_OUT,
    };

    STATUS_CODE_REGISTER Register(const std::string& login, const std::string& password)
    {
        if(UserExists(login))
            return REGISTER_STATUS_ALREADY_REGISTERED;
        Users.insert(std::make_pair(login, password));
        return REGISTER_STATUS_OK;
    }
    STATUS_CODE_LOGIN Login(const std::string& login, const std::string& password)
    {
        if(!UserExists(login))
            return LOGIN_STATUS_NO_USER;
        if(Users[login] != password)
            return LOGIN_STATUS_INCORRECT_PASS;
        if(UserLoggedIn(login))
            return LOGIN_STATUS_ALREADY_LOGGED;
        LoggedInUsers.push_back(login);
        return LOGIN_STATUS_OK;
    }
    STATUS_CODE_LOGOUT Logout(std::string login)
    {
        if(!UserExists(login))
            return LOGOUT_STATUS_NO_USER;
        if(!UserLoggedIn(login))
            return LOGOUT_STATUS_ALREADY_LOGGED_OUT;
        LoggedInUsers.erase(std::find(LoggedInUsers.begin(), LoggedInUsers.end(),login));
        return LOGOUT_STATUS_OK;
    }
};


#endif //REGISTRATION_ACCOUNTSMANAGER_H
