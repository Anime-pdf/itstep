cmake_minimum_required(VERSION 3.24)
project(registration)

set(CMAKE_CXX_STANDARD 17)

add_executable(registration main.cpp AccountsManager.h)
