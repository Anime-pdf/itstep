#ifndef BINARYTREE_BINARYTREE_H
#define BINARYTREE_BINARYTREE_H

#include <iostream>

namespace MySTL {

    template<typename T>
    class BinaryNode {
    public:
        T value;
        BinaryNode* left;
        BinaryNode* right;
        explicit BinaryNode(T v) {value = v; left = right = nullptr;}
        void showNode() {std::cout << value << std::endl << "Left: " << ((left==nullptr) ? 0 : left->value) << "; Right: " << ((right==nullptr) ? 0 : right->value) << std::endl; }
    };

    template<typename T>
    class BinaryTree {
        BinaryNode<T>* root;
    public:
        BinaryTree() { root = nullptr; }
        ~BinaryTree() = default;

        void insert(T v) {
            auto my_node = new BinaryNode<T>(v);
            if(root == nullptr) root = my_node;
            else {
                BinaryNode<T>* p = root;
                BinaryNode<T>* parent = root;
                while (p != nullptr)
                {
                    parent = p;
                    if(v < p->value)
                        p = p->left;
                    else
                        p = p->right;
                }
                if(v < parent->value)
                    parent->left = my_node;
                else
                    parent->right = my_node;
            }
        }

        BinaryNode<T>* search(T value) {
            if(root != nullptr) {
                auto p = root;
                while (p != nullptr)
                {
                    if(p->value == value)
                        return p;
                    else if(value < p->value)
                        p = p->left;
                    else
                        p = p->right;
                }
            }
            return nullptr;
        }

        void showF(BinaryNode<T>* p) {
            if (p != nullptr) {
                showF(p->left);
                std::cout << p->value << " ";
                showF(p->right);
            }
        }

        void showTree() {
            if (root == nullptr)
                std::cout << "Bin tree is empty!" << std::endl;
            else {
                showF(root->left);
                std::cout << root->value << " ";
                showF(root->right);
                std::cout << std::endl;
            }
        }
    };

} // MySTL

#endif //BINARYTREE_BINARYTREE_H
