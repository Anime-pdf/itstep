#include <iostream>
#include <list>
#include <forward_list>
#include <algorithm>
#include "src/SingleLinkedList.h"
#include "src/BidirectionalLinkedList.h"

template<typename T>
void printList(T lst)
{
    printf("\nNew List:\n");
    auto p = lst._head;
    while (p != nullptr) {
        std::cout << p->_value << " ";
        p = p->_next;
    }
}

int main() {
    MySTL::SingleLinkedList<int> sss;
    sss.add(123);
    sss.add(182412);
    sss.add(1244);
    sss.add(9151);

    printList(sss);

    sss.remove(123);
    sss.remove(1244);

    printList(sss);

    sss.reverse();

    printList(sss);

    printf("\n\nDOUBLE!!!!!\n\n");

    MySTL::BidirectionalLinkedList<int> ss;
    ss.add(123);
    ss.add(182412);
    ss.add(1244);
    ss.add(9151);

    printList(ss);

    ss.remove(123);
    ss.remove(1244);

    printList(ss);

    ss.reverse();

    printList(ss);

    // stl lists
    printf("\n\nSTL!!!!!\n\n");

    std::list<int> s_list;

    s_list.push_back(12);
    s_list.push_back(55);
    s_list.push_back(112);
    s_list.push_back(6);
    s_list.push_back(-4);

    std::cout << &*std::max_element(s_list.begin(), s_list.end());

    std::replace_if(s_list.begin(), s_list.end(), [&](auto &item){return item<0;}, 0);

    printf("\nNew list:\n");
    for (const auto &item: s_list)
    {
        std::cout << item << " ";
    }

    {
        int max = *std::max_element(s_list.begin(), s_list.end());
        std::for_each(s_list.begin(), s_list.end(), [&](auto &item) {item+=max;});
    }

    printf("\nNew list:\n");
    for (const auto &item: s_list)
    {
        std::cout << item << " ";
    }

    s_list.remove(118);

    printf("\nNew list:\n");
    for (const auto &item: s_list)
    {
        std::cout << item << " ";
    }

    s_list.insert(std::find(s_list.begin(), s_list.end(),167), 2);

    printf("\nNew list:\n");
    for (const auto &item: s_list)
    {
        std::cout << item << " ";
    }

    //4
    printf("\n\nFWRD!!!!!\n\n");

    struct Product
    {
        std::string name;
        float price;
        struct {
            int d,m,y;
        } date;
    };

    std::forward_list<Product> bd_list;

    bd_list.push_front({"Idk", 123.4f, {3,2,2023}});
    bd_list.push_front({"Idk2", 1.4f, {13,5,2023}});
    bd_list.push_front({"3Idk", 51.4f, {2,2,2021}});
    bd_list.push_front({"Id4k", 12.4f, {27,6,2024}});

    printf("\nNew list:\n");
    for (const auto &item: bd_list)
    {
        std::cout << item.name << " " << item.price << " " << item.date.d << "/" << item.date.m << "/" << item.date.y << "\n";
    }

    int cur_d = 3, cur_m = 2, cur_y = 2023; // ok?
    std::remove_if(bd_list.begin(), bd_list.end(), [&](auto &item) -> bool {return (item.date.y < cur_y || (item.date.y == cur_y && item.date.m < cur_m) || (item.date.y == cur_y && item.date.m < cur_m && item.date.d < cur_d));});

    printf("\nNew list:\n");
    for (const auto &item: bd_list)
    {
        std::cout << item.name << " " << item.price << " " << item.date.d << "/" << item.date.m << "/" << item.date.y << "\n";
    }

    return 0;
}
