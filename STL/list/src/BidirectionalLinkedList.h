#ifndef LIST_BIDIRECTIONALLINKEDLIST_H
#define LIST_BIDIRECTIONALLINKEDLIST_H

namespace MySTL {
    template<typename T>
    class BiNode {
    public:
        T _value;
        BiNode *_next;
        BiNode *_prev;

        BiNode(T a) {
            _value = a;
            _next = nullptr;
            _prev = nullptr;
        }
    };

    template<typename T>
    class BidirectionalLinkedList {
    public:
        BiNode<T> *_head;
        BiNode<T> *_tail;
        int _length;
        BidirectionalLinkedList() {
            _head = nullptr;
            _tail = nullptr;
            _length = 0;
        }

        void add(T a) {
            auto *node = new BiNode<T>(a);
            if (!_head && !_tail) {
                _head = node;
                _tail = _head;
            } else {
                _tail->_next = node;
                node->_prev = _tail;
                _tail = _tail->_next;
            }
            _length++;
        }

        void remove(T a) {
            if (contains(a)) {
                auto current = _head;
                auto previous = _head;
                while (current) {
                    if (current->_value == a) {
                        if (_head == current) {
                            _head = _head->_next;
                            _head->_prev = nullptr;
                            _length--;
                            return;
                        }
                        if (_tail == current) { _tail = previous; _tail->_next = nullptr; }
                        previous->_next = current->_next;
                        current->_next = previous;
                        _length--;
                        return;
                    }
                    previous = current;
                    current = current->_next;
                }
            }
        }

        void reverse()
        {
            BiNode<T>* current = _head;
            BiNode<T>* prev = nullptr, *next = nullptr;

            while (current != nullptr) {
                next = current->_next;
                current->_next = prev;
                prev = current;
                current = next;
            }
            _head = prev;
        }

        bool contains(T a) {
            auto node = _head;
            while (node) {
                if (node->_value == a) { return true; }
                node = node->_next;
            }
            return false;
        }

        int size() { return _length; }
    };
} // MySTL

#endif //LIST_BIDIRECTIONALLINKEDLIST_H
