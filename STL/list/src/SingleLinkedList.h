#ifndef LIST_SINGLELINKEDLIST_H
#define LIST_SINGLELINKEDLIST_H

namespace MySTL {
    template<typename T>
    class SingleNode {
    public:
        T _value;
        SingleNode *_next;
        SingleNode(T a) {
            _value = a;
            _next = nullptr;
        }
    };

    template<typename T>
    class SingleLinkedList {
    public:
        SingleNode<T> *_head;
        SingleNode<T> *_tail;
        int _length;
        SingleLinkedList() {
            _head = nullptr;
            _tail = nullptr;
            _length = 0;
        }

        void add(T a) {
            auto node = new SingleNode(a);
            if (!_head && !_tail) {
                _head = node;
                _tail = _head;
            } else {
                _tail->_next = node;
                _tail = _tail->_next;
            }
            _length++;
        }

        void remove(T a) {
            if (contains(a)) {
                auto current = _head;
                auto previous = _head;
                while (current) {
                    if (current->_value == a) {
                        if (_head == current) {
                            _head = _head->_next;
                            _length--;
                            return;
                        }
                        if (_tail == current) { _tail = previous; }
                        previous->_next = current->_next;
                        _length--;
                        return;
                    }
                    previous = current;
                    current = current->_next;
                }
            }
        }

        void reverse()
        {
            auto current = _head;
            SingleNode<T> *prev = nullptr, *next = nullptr;

            while (current != nullptr) {
                next = current->_next;
                current->_next = prev;
                prev = current;
                current = next;
            }
            _head = prev;
        }

        bool contains(T a) {
            auto node = _head;
            while (node) {
                if (node->_value == a) { return true; }
                node = node->_next;
            }
            return false;
        }

        int size() { return _length; }
    };

} // MySTL

#endif //LIST_SINGLELINKEDLIST_H
