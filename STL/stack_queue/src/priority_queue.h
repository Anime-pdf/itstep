#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include <algorithm>

namespace MySTL {

    template<class T>
    class priority_queue {
        T *array_;
        int size_;
    public:
        priority_queue() : array_(nullptr), size_(0) {}
        ~priority_queue() { delete[] array_; }

        bool empty() { return size_ == 0; }
        int size() { return size_; }
        T front() { return array_[0]; }
        T back() { return array_[size_-1]; }
        void push(T item)
        {
            T* new_arr = new T[size_ + 1];
            for (int i = 0; i < size_; ++i)
                new_arr[i] = array_[i];
            new_arr[size_++] = item;
            array_ = new_arr;
            std::sort(array_, array_+size_, std::greater<T>());
        }
        void pop()
        {
            T* new_arr = new T[--size_];
            for (int i = 0; i < size_; ++i)
                new_arr[i] = array_[i+1];
            array_ = new_arr;
        }
    };

} // MySTL

#endif //PRIORITY_QUEUE_H
