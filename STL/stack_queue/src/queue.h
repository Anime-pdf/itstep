#ifndef QUEUE_H
#define QUEUE_H

namespace MySTL {

    template<class T>
    class queue {
        T *array_;
        int size_;
    public:
        queue() : array_(nullptr), size_(0) {}
        ~queue() { delete[] array_; }

        bool empty() { return size_ == 0; }
        int size() { return size_; }
        T front() { return array_[0]; }
        void push(T item)
        {
            T* new_arr = new T[size_ + 1];
            for (int i = 0; i < size_; ++i)
                new_arr[i] = array_[i];
            new_arr[size_++] = item;
            array_ = new_arr;
        }
        void pop()
        {
            T* new_arr = new T[--size_];
            for (int i = 0; i < size_; ++i)
                new_arr[i] = array_[i+1];
            array_ = new_arr;
        }
    };

} // MySTL

#endif //QUEUE_H
