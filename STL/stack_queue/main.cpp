#include <iostream>
#include <stack>
#include "src/priority_queue.h"

int main() {

    MySTL::priority_queue<int> q;
    q.push(-22);
    q.push(22);
    q.push(0);
    q.push(215);
    q.push(-1);

    while (!q.empty())
    {
        std::cout << q.front() << " ";
        q.pop();
    }

    // Output: 215 22 0 -1 -22

    int ch;
    std::cin >> ch;

    if(ch == 1) {
        std::stack<bool> out;

        int in;
        std::cin >> in;
        std::cout << in << " = ";
        while (in > 1) {
            out.push(in % 2);
            in /= 2;
        }
        out.push(in);

        while (!out.empty()) {
            std::cout << out.top();
            out.pop();
        }
    }
    else if (ch == 2)
    {
        std::stack<char> out;
        std::string in;
        std::cin >> in;

        for (int i = 0; i < in.size()/2; ++i)
            out.push(in[i]);

        int i = in.size() / 2;
        if(in.size()%2!=0)
            i++;
        for (i; i < in.size(); ++i) {
            if (in[i] != out.top()) {
                std::cout << "Not poly";
                return 0;
            }
            out.pop();
        }
        std::cout << "Is poly!";
    }
    return 0;
}
