#include <iostream>
#include <map>
#include <string>
#include "src/TodoList.h"

int main() {
    // 1
    int ch;
    std::cin >> ch;
    std::cin.ignore();

    if(ch == 1)
    {
        std::map<std::string, int> words;

        std::string input;
        std::getline(std::cin, input);
        input += " ";

        size_t pos = 0;
        std::string token;
        while ((pos = input.find(" ")) != std::string::npos) {
            token = input.substr(0, pos);
            if(words.find(token) != words.end())
                words[token]++;
            else
                words.insert(std::make_pair(token, 1));
            input.erase(0, pos + 1);
        }

        printf("\n");
        for (const auto &item: words)
        {
            std::cout << item.first << " - " << item.second << std::endl;
        }
    }
    else if(ch == 2)
    {
        TodoList list;
        auto d1 = Date();
        auto d2 = Date()+122;
        auto d3 = Date()+222;

        list.addTask(d1, "eat smth");
        list.addTask(d1, "do smth");
        list.addTask(d1, "drink smth");
        list.addTask(d2, "eat smth");
        list.addTask(d3, "do smth");
        list.addTask(d3, "drink smth");

        std::cout << "Todo for "<< d1 << " :\n";
        for (const auto &item: list.getList(d1))
        {
            std::cout << item << std::endl;
        }
        std::cout << "Todo for "<< d2 << " :\n";
        for (const auto &item: list.getList(d2))
        {
            std::cout << item << std::endl;
        }
        std::cout << "Todo for "<< d3 << " :\n";
        for (const auto &item: list.getList(d3))
        {
            std::cout << item << std::endl;
        }
    }

    return 0;
}
