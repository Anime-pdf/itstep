#ifndef MAPPRAC_TODOLIST_H
#define MAPPRAC_TODOLIST_H


#include <map>
#include <vector>
#include "Date.h"

class TodoList {
    std::map<Date, std::vector<std::string>> list_;
public:
    TodoList() = default;

    void addTask(Date d, std::string task)
    {
        list_[d].push_back(task);
    }
    std::vector<std::string> getList(Date d)
    {
        return list_[d];
    }
};


#endif //MAPPRAC_TODOLIST_H
