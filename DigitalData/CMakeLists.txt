cmake_minimum_required(VERSION 3.23)
project(DigitalData)

set(CMAKE_CXX_STANDARD 20)

add_executable(DigitalData main.cpp src/Item.h src/MovieDvd.h src/MusicCd.h)
