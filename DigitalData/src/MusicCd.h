#ifndef DIGITALDATA_MUSICCD_H
#define DIGITALDATA_MUSICCD_H

#include <iostream>
#include "Item.h"
using std::cout;
using std::endl;
using std::string;

class MusicCD : public Item {
protected:
    int minutes;
public:
    void show() const{
        cout << "------------MusicCD------------" << endl;
        Item::show();
        cout << "Minutes: " << minutes << endl;
    }
    MusicCD(const string &name, const string &author, int year, float price, int minutes) : Item(name, author,
                                                                                                          year, price),
                                                                                                     minutes(minutes) {
        this->minutes = minutes;
    }
};

#endif //DIGITALDATA_MUSICCD_H
