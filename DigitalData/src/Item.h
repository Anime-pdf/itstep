#ifndef DIGITALDATA_ITEM_H
#define DIGITALDATA_ITEM_H

#include <string>

class Item {
protected:
    std::string title_;
    std::string author_;
    float price_;
    int year_;
public:
    Item(std::string title = "Item", std::string author = "Me", float price = 20, int year = 2000) : title_(title), author_(author) {
        setPrice(price);
        setYear(year);
    }

    bool setPrice(float price) { if (price < 0) return false; price_ = price; return true; }
    bool setYear(int year) { if (year < 0) return false; year_ = year; return true; }

    void show() const {

    }
};

#endif //DIGITALDATA_ITEM_H
