#ifndef DIGITALDATA_MOVIEDVD_H
#define DIGITALDATA_MOVIEDVD_H

#include <iostream>
#include "Item.h"
#include "MusicCd.h"

using std::cout;
using std::string;

enum Genre{Drama = 1, Comedy, Series, Action};
class MovieDVD : public MusicCD {
    Genre genre;
    string country;
    string actors;
public:
    void show() const {
        cout << "------------MovieDVD------------" << endl;
        Item::show();
        cout << "Minutes: " << minutes << endl;
        cout << "Genre: ";
        switch (genre) {
            case Drama:
                cout << "Drama\n";
                break;
            case Comedy:
                cout << "Comedy\n";
                break;
            case Series:
                cout << "Series\n";
                break;
            case Action:
                cout << "Action\n";
                break;
        }
        cout << "Country: " << country << endl;
        cout << "Actors: " << actors << endl;
    }

    float getPrice() const {
        switch (genre) {
            case Drama:
                return price_-price_*0.15;
            case Comedy:
                return price_-price_*0.1;
            case Series:
                return price_-price_*0.2;
            case Action:
                return price_;
        }
        return 0;
    }
    MovieDVD(const string &name, const string &author, int year, float price, int minutes, Genre genre,
                       const string &country, const string &actors) : MusicCD(name, author, year, price, minutes){
        this->genre = genre;
        this->country = country;
        this->actors = actors;
    }
};
#endif //DIGITALDATA_MOVIEDVD_H
