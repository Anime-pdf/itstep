#include <iostream>
#include "src/MultiFun.h"

int main() {
    MultiFun a;
    a.Printer::setText("Test");
    a.Printer::printText("lol.txt");
    a.Scanner::scanText("lol.txt");
    a.Scanner::setText("Testtt2");
    a.copyText();
    a.Printer::printText("lol.txt");
    a.Scanner::scanText("lol.txt");
    std::cout << a.Scanner::getText() << std::endl;
    return 0;
}