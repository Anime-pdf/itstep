#ifndef MFU_SCANNER_H
#define MFU_SCANNER_H

#include <string>
#include <fstream>

class Scanner {
    std::string _text;
public:
    void scanText(const std::string& filename) { std::ifstream file(filename); if (file.is_open()) { _text = std::string((std::istreambuf_iterator<char>(file)),std::istreambuf_iterator<char>()); file.close(); }}
    void setText(const std::string& text) { _text = text; }
    std::string getText() const { return _text; }
};

#endif //MFU_SCANNER_H
