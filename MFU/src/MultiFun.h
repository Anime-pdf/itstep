#ifndef MFU_MULTIFUN_H
#define MFU_MULTIFUN_H

#include "Printer.h"
#include "Scanner.h"

class MultiFun : public Printer, public Scanner{
public:
    void copyText() { Printer::setText(Scanner::getText()); }
};

#endif //MFU_MULTIFUN_H
