#ifndef MFU_PRINTER_H
#define MFU_PRINTER_H

#include <string>
#include <fstream>

class Printer {
    std::string _text;
public:
    void printText(const std::string& filename) const { std::ofstream file(filename); if (file.is_open()) { file << _text; file.close(); }}
    std::string getText() const { return _text; }
    void setText(const std::string& text) { _text = text; }
};

#endif //MFU_PRINTER_H
