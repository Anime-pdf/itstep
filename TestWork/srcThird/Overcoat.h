#ifndef TESTWORK_OVERCOAT_H
#define TESTWORK_OVERCOAT_H

#include <string>

class Overcoat {
public:
    enum Type {
        TYPE_MAN = 0,
        TYPE_WOMAN,
        TYPE_CHILD,
    };
    enum Size {
        SIZE_XS = 0,
        SIZE_S,
        SIZE_M,
        SIZE_L,
        SIZE_XL,
        SIZE_2XL,
    };
private:
    static void clamp(Type& type){
        if (type > TYPE_CHILD)
            type = TYPE_CHILD;
        if(type < TYPE_MAN)
            type = TYPE_MAN;
    }
    static void clamp(Size& size){
        if (size > SIZE_2XL)
            size = SIZE_2XL;
        if(size < SIZE_XS)
            size = SIZE_XS;
    }
    static std::string typeToStr(Type type){
        switch (type) {
            case TYPE_MAN:
                return "Man";
            case TYPE_WOMAN:
                return "Woman";
            case TYPE_CHILD:
                return "Child";
        }
    }
    static std::string sizeToStr(Size size){
        switch (size) {
            case SIZE_XS:
                return "Extra Small";
            case SIZE_S:
                return "Small";
            case SIZE_M:
                return "Middle";
            case SIZE_L:
                return "Large";
            case SIZE_XL:
                return "Extra Large";
            case SIZE_2XL:
                return "Extra Extra Large";
        }
    }

    std::string _name;
    Type _type;
    Size _size;
    float _price;
public:
    explicit Overcoat(const std::string& name = "Winter Coat", Type type = TYPE_MAN, Size size = SIZE_XL, float price = 4500.f);
    Overcoat(const Overcoat& obj); //Копирования
    Overcoat& operator = (const Overcoat& obj);

    //---------АКСЕССОРЫ---------
    bool setName(const std::string& name);
    std::string getName()const;
    bool setType(Type type);
    Type getType()const;
    bool setSize(Size size);
    Size getSize()const;
    bool setPrice(float price);
    float getPrice()const;

    void showOvercoat()const;
    std::string returnFormatted() const;
    bool valid()const; //Проверка на корректность

    //--------- ОПЕРАТОРЫ СРАВНЕНИЯ ---------
    bool operator == (const Overcoat& obj)const&;
    bool operator != (const Overcoat& obj)const&;
    bool operator > (const Overcoat& obj)const&;
    bool operator < (const Overcoat& obj)const&;
    bool operator >= (const Overcoat& obj)const&;
    bool operator <= (const Overcoat& obj)const&;

#if __cplusplus >= 202002L
    int operator <=> (const Overcoat& obj)const&;
#else
    int threeWayComparison(const Overcoat& obj)const&;
#endif

    //--------- ОПЕРАТОРЫ ПРИСВОЕНИЯ ---------
    Overcoat& operator += (int n);
    Overcoat& operator -= (int n);

    //--------- АРИФМЕТИЧЕСКИЕ ОПЕРАТОРЫ ---------
    Overcoat operator + (int n)const&;
    Overcoat operator - (int n)const&;

    Overcoat& operator -- ();
    Overcoat operator -- (int n);
    Overcoat& operator ++ ();
    Overcoat operator ++ (int n);

    friend Overcoat operator + (int n, Overcoat a);
    friend Overcoat operator - (int n, Overcoat a);

    friend std::ostream& operator << (std::ostream& os, const Overcoat& t);
    friend std::istream& operator >> (std::istream& is, Overcoat& t);
};

#endif //TESTWORK_OVERCOAT_H
