#ifndef TESTWORK_SHOP_H
#define TESTWORK_SHOP_H

#include <utility>
#include <algorithm>
#include <vector>
#include "Overcoat.h"

class Shop {
    std::vector<Overcoat> Clothes;
public:
    Shop() = default;
    explicit Shop(std::vector<Overcoat> clothes) : Clothes(std::move(clothes)) {}

    void add(const Overcoat& item) { Clothes.push_back(item); }
    void remove(const std::string& name) { erase_if(Clothes, [name](const Overcoat& item){ if (item.getName() == name) return true; return false;}); }
    void find(Overcoat::Type type) { std::for_each(Clothes.begin(), Clothes.end(), [type](const Overcoat& item) { if (item.getType() == type) { item.showOvercoat(); printf("\n"); }}); }
    void changeSize(const std::string& name, Overcoat::Size size) {std::find_if(Clothes.begin(), Clothes.end(), [name](const Overcoat& item) {if (item.getName() == name) return true; return false;})->setSize(size); }
    void sort() { std::sort(Clothes.begin(), Clothes.end(), [](Overcoat& itemL, Overcoat& itemR) {if(itemL.getPrice() < itemR.getPrice()) return true; return false; });}

    void show() const {
        for (const auto &item: Clothes) {
            item.showOvercoat();
            printf("\n");
        }
        printf("\n");
    }
};

#endif //TESTWORK_SHOP_H
