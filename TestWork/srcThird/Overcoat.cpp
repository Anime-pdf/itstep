#include <sstream>
#include "Overcoat.h"

Overcoat::Overcoat(const std::string& name, Overcoat::Type type, Overcoat::Size size, float price) {
    setName(name);
    setPrice(price);
    setSize(size);
    setType(type);
}

Overcoat::Overcoat(const Overcoat &obj) {
    setName(obj._name);
    setPrice(obj._price);
    setSize(obj._size);
    setType(obj._type);
}

Overcoat &Overcoat::operator=(const Overcoat &obj) {
    setName(obj._name);
    setPrice(obj._price);
    setSize(obj._size);
    setType(obj._type);
    return *this;
}

// ===

bool Overcoat::setName(const std::string& name) {
    if (name.empty())
        return false;
    _name = name;
    return true;
}

std::string Overcoat::getName() const {
    return _name;
}

bool Overcoat::setType(Overcoat::Type type) {
    if (type > TYPE_CHILD || type < TYPE_MAN)
        return false;
    _type = type;
    return true;
}

Overcoat::Type Overcoat::getType() const {
    return _type;
}

bool Overcoat::setSize(Overcoat::Size size) {
    if (size > SIZE_2XL || size < SIZE_XS)
        return false;
    _size = size;
    return true;
}

Overcoat::Size Overcoat::getSize() const {
    return _size;
}

bool Overcoat::setPrice(float price) {
    if (price < 0)
        return false;
    _price = price;
    return true;
}

float Overcoat::getPrice() const {
    return _price;
}

// ===

void Overcoat::showOvercoat() const {
    printf("%s", returnFormatted().c_str());
}

std::string Overcoat::returnFormatted() const {
    std::stringstream ss;
    ss << "Name: " << _name << "\n" << "Type: " << typeToStr(_type) << "\n" << "Size: " << sizeToStr(_size) << "\n" << "Price: " << _price << " UAH\n";
    return ss.str();
}

bool Overcoat::valid() const {
    if (_name.empty())
        return false;
    if (_type > TYPE_CHILD || _type < TYPE_MAN)
        return false;
    if (_size > SIZE_2XL || _size < SIZE_XS)
        return false;
    if (_price < 0)
        return false;
    return true;
}

bool Overcoat::operator==(const Overcoat &obj) const &{
    return _type == obj._type;
}

bool Overcoat::operator!=(const Overcoat &obj) const &{
    return _type != obj._type;;
}

bool Overcoat::operator>(const Overcoat &obj) const &{
    return _price > obj._price;
}

bool Overcoat::operator<(const Overcoat &obj) const &{
    return _price < obj._price;
}

bool Overcoat::operator>=(const Overcoat &obj) const &{
    return _price >= obj._price;
}

bool Overcoat::operator<=(const Overcoat &obj) const &{
    return _price <= obj._price;
}

#if __cplusplus >= 202002L
int Overcoat::operator<=>(const Overcoat &obj) const &{
#else
    int Overcoat::threeWayComparison(const Overcoat& obj)const&{
#endif
    if (*this < obj) return -1;
    if (*this > obj) return 1;
    return 0;
}

Overcoat &Overcoat::operator+=(int n) {
    Size nn = static_cast<Size>(_size+n);
    clamp(nn);
    _size = nn;
    return *this;
}

Overcoat &Overcoat::operator-=(int n) {
    Size nn = static_cast<Size>(_size-n);
    clamp(nn);
    _size = nn;
    return *this;
}

Overcoat Overcoat::operator+(int n) const &{
    Size nn = static_cast<Size>(_size+n);
    clamp(nn);
    Overcoat tmp = *this;
    tmp._size = nn;
    return tmp;
}

Overcoat Overcoat::operator-(int n) const &{
    Size nn = static_cast<Size>(_size-n);
    clamp(nn);
    Overcoat tmp = *this;
    tmp._size = nn;
    return tmp;
}

Overcoat &Overcoat::operator--() {
    Size nn = static_cast<Size>(_size-1);
    clamp(nn);
    _size = nn;
    return *this;
}

Overcoat Overcoat::operator--(int n) {
    Overcoat tmp = *this;
    Size nn = static_cast<Size>(_size-1);
    clamp(nn);
    _size = nn;
    return tmp;
}

Overcoat &Overcoat::operator++() {
    Size nn = static_cast<Size>(_size+1);
    clamp(nn);
    _size = nn;
    return *this;
}

Overcoat Overcoat::operator++(int n) {
    Overcoat tmp = *this;
    Size nn = static_cast<Size>(_size+1);
    clamp(nn);
    _size = nn;
    return tmp;
}

Overcoat operator+(int n, Overcoat a) {
    Overcoat::Size nn = static_cast<Overcoat::Size>(a._size+n);
    Overcoat::clamp(nn);
    Overcoat tmp = a;
    tmp._size = nn;
    return tmp;
}

Overcoat operator-(int n, Overcoat a) {
    Overcoat::Size nn = static_cast<Overcoat::Size>(a._size+n);
    Overcoat::clamp(nn);
    Overcoat tmp = a;
    tmp._size = nn;
    return tmp;
}

std::ostream &operator<<(std::ostream &os, const Overcoat &t) {
    os << t.returnFormatted();
    return os;
}

std::istream &operator>>(std::istream &is, Overcoat &t) {
    std::string name;
    int type, size;
    float price;
    do {
        printf("Enter Name: ");
        is >> name;
    } while (!t.setName(name));
    do {
        printf("Enter Type: ");
        is >> type;
    } while (!t.setType(static_cast<Overcoat::Type>(type)));
    do {
        printf("Enter Size: ");
        is >> size;
    } while (!t.setSize(static_cast<Overcoat::Size>(size)));
    do {
        printf("Enter Price: ");
        is >> price;
    } while (!t.setPrice(price));
    return is;
}
