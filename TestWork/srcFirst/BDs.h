#ifndef TESTWORK_BDS_H
#define TESTWORK_BDS_H

#include <cstdio>

// Basic classes

class B1{
    int _b1;
public:
    B1(int b1) {_b1 = b1;}

    virtual  int getValue() const { return _b1; }

    virtual void show() const { printf("B1: %d ", _b1);}
};

class B2{
    int _b2;
public:
    B2(int b2) {_b2 = b2;}

    virtual int getValue() const { return _b2; }

    virtual void show() const { printf("B2: %d ", _b2);}
};

class B3{
    int _b3;
public:
    B3(int b3) {_b3 = b3;}

    virtual int getValue() const { return _b3; }

    virtual void show() const { printf("B3: %d ", _b3);}
};

// inheritance

class D1 : public B2, protected B1 {
    int _d1;
public:
    D1(int d1, int b1, int b2) : B1(b1), B2(b2) {_d1 = d1;}

    virtual int getValue() const { return _d1; }

    virtual void show() const { printf("D1: %d ", _d1); B2::show(); B1::show();}
};


class D2 : public D1, protected B3 {
    int _d2;
public:
    D2(int d2, int d1, int b1, int b2, int b3) : D1(d1, b1, b2), B3(b3) {_d2 = d2;}

    virtual int getValue() const { return _d2; }

    virtual void show() const { printf("D2: %d ", _d2); D1::show(); B3::show();}
};

class D3 : public D2 {
    int _d3;
public:
    D3(int d3, int d2, int d1, int b1, int b2, int b3) : D2(d2, d1, b1, b2, b3) {_d3 = d3;}

    virtual int getValue() const { return _d3; }

    virtual void show() const { printf("D3: %d ", _d3); D2::show();}
};

#endif //TESTWORK_BDS_H
