#ifndef TESTWORK_HUMAN_H
#define TESTWORK_HUMAN_H

#include <string>
#include <utility>
#include "src/Date.h"

class Human {
    std::string _name;
    Date _birthday;
public:
    Human(std::string name, const Date& birthday = Date::getCurrentDate()) : _name(std::move(name)), _birthday(birthday) {}

    std::string getName() const {return _name;}
    Date getBirthday() const {return _birthday;}

    bool setName(const std::string& name) { if (name.empty()) return false; _name = name; return true;}
};

#endif //TESTWORK_HUMAN_H
