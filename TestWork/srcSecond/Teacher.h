#ifndef TESTWORK_TEACHER_H
#define TESTWORK_TEACHER_H

#include "Human.h"

class Teacher : Human {
    std::string _job;
    std::string _department;
public:
    Teacher(const std::string& job,
            const std::string& department,
            const std::string& name,
            const Date& birthday = Date::getCurrentDate())
            : Human(name, birthday)
    {
        setDepartment(job);
        setJob(department);
    }

    std::string getDepartment() const {return _department;}
    std::string getJob() const {return _job;}

    bool setDepartment(const std::string& department) { if (department.empty()) return false; _department = department; return true;}
    bool setJob(const std::string& job) { if (job.empty()) return false; _job = job; return true;}
};

#endif //TESTWORK_TEACHER_H
