#ifndef TESTWORK_STUDENT_H
#define TESTWORK_STUDENT_H

#include "Enrollee.h"

class Student : Enrollee {
    int _year;
    std::string _group;
    std::string _faculty;
public:
    Student(int year,
            const std::string& group,
            const std::string& faculty,
            float mark,
            const std::string& name,
            const Date& birthday = Date::getCurrentDate())
            : Enrollee(mark, name, birthday)
    {
        setYear(year);
        setGroup(group);
        setFaculty(faculty);
    }

    int getYear() const {return _year;}
    std::string getGroup() const {return _group;}
    std::string getFaculty() const {return _faculty;}

    bool setYear(int year) { if (year <= 0 || year > 5) return false; _year = year; return true;}
    bool setGroup(const std::string& group) { if (group.empty()) return false; _group = group; return true;}
    bool setFaculty(const std::string& faculty) { if (faculty.empty()) return false; _faculty = faculty; return true;}
};

#endif //TESTWORK_STUDENT_H
