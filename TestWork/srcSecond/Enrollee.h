#ifndef TESTWORK_ENROLLEE_H
#define TESTWORK_ENROLLEE_H

#include <utility>

#include "Human.h"

class Enrollee : Human {
    float _mark;
public:
    Enrollee(float mark, std::string name, const Date& birthday = Date::getCurrentDate()) : Human(std::move(name), birthday) { setMark(mark); }

    float getMark() const {return _mark;}

    bool setMark(float mark) { if (mark <= 0) return false; _mark = mark; return true;}
};

#endif //TESTWORK_ENROLLEE_H
