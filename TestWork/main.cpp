#include <iostream>
#include "srcFirst/BDs.h"
#include "srcSecond/Student.h"
#include "srcSecond/Teacher.h"
#include "srcThird/Shop.h"

int main() {
    D3 a(1,2,3,4,5,6);
    a.show();
    printf("\n==========\n");

    Teacher t("Teacher", "Math", "John");
    Student s(2,"SSS", "Math", 99.2f, "Bob");
    // тесты? не, не крашит - и слава богу

    Shop shop;
    shop.add(Overcoat());
    shop.add(Overcoat("Winter Lol"));

    shop.show();

    shop.remove("Winter Lol");

    shop.show();

    shop.add(Overcoat("Winter Kek", Overcoat::TYPE_MAN, Overcoat::SIZE_2XL, 5000));
    shop.add(Overcoat("Winter Lol", Overcoat::TYPE_MAN, Overcoat::SIZE_2XL, 6000));
    shop.add(Overcoat("Winter $ebuek", Overcoat::TYPE_MAN, Overcoat::SIZE_2XL, 2000));

    shop.sort();
    shop.show();

    return 0;
}
