#ifndef DATE_H
#define DATE_H

#include <iostream>
using std::istream;
using std::ostream;

class Date
{
	int year;
	int month;
	int day;

	bool isLeapYear() const; //Проверка на високосный год
	bool isLeapYear(int y) const; //Проверка на високосный год
	int monthDays() const; //Количество дней по месяцу
	int monthDays(int m) const; //Количество дней по месяцу
	void nextDate(); //Следующая дата
	void prevDate(); //Предыдущая дата

public:
    static Date getCurrentDate();

	//---------КОНСТРУКТОРЫ и ДЕСТРУКТОР---------
	Date(); // По умолчанию - текущая дата
	Date(int year, int month, int day); //С параметрами - заданная дата
	Date(const Date& obj); //Копирования
    Date& operator = (const Date& obj);

	//---------АКСЕССОРЫ---------
	bool setYear(int y);
	int getYear()const;
    bool setMonth(int month);
	int getMonth()const;
    bool setDay(int day);
	int getDay()const;

	void showDate()const;
    std::string returnFormatted() const;
	bool valid()const; //Проверка даты на корректность

	//--------- ОПЕРАТОРЫ СРАВНЕНИЯ ---------
	bool operator == (const Date& obj)const&;
	bool operator != (const Date& obj)const&;
	bool operator > (const Date& obj)const&;
	bool operator < (const Date& obj)const&;
	bool operator >= (const Date& obj)const&;
	bool operator <= (const Date& obj)const&;

#if __cplusplus >= 202002L
	int operator <=> (const Date& obj)const&;
#else
    int threeWayComparison(const Date& obj)const&;
#endif
    bool isBetween(const Date& obj1, const Date& obj2) const&;

	//--------- ОПЕРАТОРЫ ПРИСВОЕНИЯ ---------
	Date& operator += (int n);
	Date& operator -= (int n);

	//--------- АРИФМЕТИЧЕСКИЕ ОПЕРАТОРЫ ---------
	Date operator + (int n)const&;
	Date operator - (int n)const&;

	int operator - (const Date& obj1)const&;

    Date& operator -- ();
    Date operator -- (int n);
    Date& operator ++ ();
    Date operator ++ (int n);

    friend Date operator + (int n, Date a);
    friend Date operator - (int n, Date a);

    friend ostream& operator << (ostream& os, const Date& t);
    friend istream& operator >> (istream& is, Date& t);
};

#endif //DATE_H
