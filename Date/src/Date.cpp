#include <ctime>
#include <cstdio>
#include <cmath>
#include "Date.h"

bool Date::isLeapYear() const {
    if ((year%400==0) || ((year%100!=0)&&(year%4==0)))
        return true;
    return false;
}

bool Date::isLeapYear(int y) const {
    if ((y%400==0) || ((y%100!=0)&&(y%4==0)))
        return true;
    return false;
}

int Date::monthDays() const {
    if((month==2) && isLeapYear()){
        return 29;
    }
    else if(month==2){
        return 28;
    }
    else if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
        return 31;
    }
    else if(month==4 || month==6 || month==9 || month==11){
        return 30;
    }
    return 0;
}

int Date::monthDays(int m) const {
    if((m==2) && isLeapYear()){
        return 29;
    }
    else if(m==2){
        return 28;
    }
else if(m==1 || m==3 || m==5 || m==7 || m==8 || m==10 || m==12){
        return 31;
    }
    else if(m==4 || m==6 || m==9 || m==11){
        return 30;
    }
    return 0;
}

void Date::nextDate() {
    if(day == monthDays())
    {
        if(month == 12)
        {
            year++;
            month = 1;
        }
        else
            month++;
        day = 1;
    }
    else
        day++;
}

void Date::prevDate() {
    if(day == 1)
    {
        if(month == 1)
        {
            year--;
            month = 12;
        }
        else
            month--;
        day = monthDays();
    }
    else
        day--;
}

Date Date::getCurrentDate() {
    return Date();
}

Date::Date() {
    struct tm *tim = new tm;
    time_t tt = time(NULL);
    tim = localtime(&tt);
    setYear(tim->tm_year+1900);
    setMonth(tim->tm_mon+1);
    setDay(tim->tm_mday);
}

Date::Date(int y, int m, int d) {
    setYear(y);
    setMonth(m);
    setDay(d);
}

Date::Date(const Date &obj) {
    setYear(obj.getYear());
    setMonth(obj.getMonth());
    setDay(obj.getDay());
}

Date &Date::operator=(const Date &obj) {
    setYear(obj.getYear());
    setMonth(obj.getMonth());
    setDay(obj.getDay());
    return *this;
}

bool Date::setYear(int y) {
    if(y < 0)
        return false;
    year = y;
    return true;
}

int Date::getYear() const {
    return year;
}

bool Date::setMonth(int m) {
    if(m < 1 || m > 12)
        return false;
    month = m;
    return true;
}

int Date::getMonth() const {
    return month;
}

bool Date::setDay(int d) {
    if(d < 1 || d > monthDays())
        return false;
    day = d;
    return true;
}

int Date::getDay() const {
    return day;
}

void Date::showDate() const {
    printf("%d/%d/%d", year, month, day);
}

std::string Date::returnFormatted() const {
    char buf[100];
    snprintf(buf, sizeof(buf), "%d/%d/%d", year, month, day);
    return {buf};
}

bool Date::valid() const {
    if(year < 0)
        return false;
    if(month < 1 || month > 12)
        return false;
    if(day < 1 || day > monthDays())
        return false;
    return true;
}

bool Date::operator==(const Date &obj) const &{
    if(obj.getYear() != getYear())
        return false;
    if(obj.getMonth() != getMonth())
        return false;
    if(obj.getDay() != getDay())
        return false;
    return true;
}

bool Date::operator!=(const Date &obj) const &{
    if(obj.getYear() != getYear())
        return true;
    if(obj.getMonth() != getMonth())
        return true;
    if(obj.getDay() != getDay())
        return true;
    return false;
}

bool Date::operator>(const Date &obj) const &{
    if(getYear() > obj.getYear())
        return true;
    else if(getYear() == obj.getYear())
        if(getMonth() > obj.getMonth())
            return true;
        else if(getMonth() == obj.getMonth())
            if(getDay() > obj.getDay())
                return true;
    return false;
}

bool Date::operator<(const Date &obj) const &{
    if(getYear() < obj.getYear())
        return true;
    else if(getYear() == obj.getYear())
        if(getMonth() < obj.getMonth())
            return true;
        else if(getMonth() == obj.getMonth())
            if(getDay() < obj.getDay())
                return true;
    return false;
}

bool Date::operator>=(const Date &obj) const &{
    if(getYear() > obj.getYear())
        return true;
    else if(getYear() == obj.getYear())
        if(getMonth() > obj.getMonth())
            return true;
        else if(getMonth() == obj.getMonth())
            if(getDay() >= obj.getDay())
                return true;
    return false;
}

bool Date::operator<=(const Date &obj) const &{
    if(getYear() < obj.getYear())
        return true;
    else if(getYear() == obj.getYear())
        if(getMonth() < obj.getMonth())
            return true;
        else if(getMonth() == obj.getMonth())
            if(getDay() <= obj.getDay())
                return true;
    return false;
}

#if __cplusplus >= 202002L
int Date::operator<=>(const Date &obj) const &{
#else
int Date::threeWayComparison(const Date& obj)const&{
#endif
    if (*this < obj) return -1;
    if (*this > obj) return 1;
    return 0;
}
bool Date::isBetween(const Date &obj1, const Date &obj2) const &{
    if(obj1 < obj2)
        return (*this <= obj2 && *this >= obj1);
    else if(obj1 > obj2)
        return (*this <= obj1 && *this >= obj2);
    else
        return (*this == obj1);
}

Date &Date::operator+=(int n) {
    for (int i = 0; i < n; i++)
        nextDate();
    return *this;
}

Date &Date::operator-=(int n) {
    for (int i = 0; i < n; i++)
        prevDate();
    return *this;
}

Date Date::operator+(int n) const &{
    Date tmp = *this;
    for (int i = 0; i < n; i++)
        tmp.nextDate();
    return tmp;
}

Date Date::operator-(int n) const &{
    Date tmp = *this;
    for (int i = 0; i < n; i++)
        tmp.prevDate();
    return tmp;
}

int Date::operator-(const Date &obj1) const &{
    if(obj1==*this)
        return 0;
    bool leftBigger = *this > obj1;
    int days=0;
    if(!leftBigger)
    {
        for(int i = getYear() +1; i <= obj1.getYear(); i++)
            if(isLeapYear(i))
                days+=366;
            else
                days+=365;

        for(int i = 1; i < getMonth(); i++)
            days-=monthDays(i);
        days-=day;

        for(int i = 1; i < obj1.getMonth(); i++)
            days+=monthDays(i);
        days+=obj1.day;
    }
    else
    {
        for(int i = obj1.getYear() +1; i <= getYear(); i++)
            if(isLeapYear(i))
                days+=366;
            else
                days+=365;

        for(int i = 1; i < obj1.getMonth(); i++)
            days-=monthDays(i);
        days-=obj1.day;

        for(int i = 1; i < getMonth(); i++)
            days+=monthDays(i);
        days+=day;
    }
    return fabs(days);
}

Date &Date::operator--() {
    prevDate();
    return *this;
}

Date Date::operator--(int n) {
    Date tmp = *this;
    prevDate();
    return tmp;
}

Date &Date::operator++() {
    nextDate();
    return *this;
}

Date Date::operator++(int n) {
    Date tmp = *this;
    nextDate();
    return tmp;
}

Date operator+(int n, Date a) {
    return a+n;
}

Date operator-(int n, Date a) {
    return a+n;
}

ostream &operator<<(ostream &os, const Date &t) {
    os << t.getYear() << "/" << t.getMonth() << "/" << t.getDay();
    return os;
}

istream &operator>>(istream &is, Date &t) {
    int y,m,d;
    do {
        printf("Enter Year: ");
        is >> y;
    } while (!t.setYear(y));
    do {
        printf("Enter Month: ");
        is >> m;
    } while (!t.setMonth(m));
    do {
        printf("Enter Day: ");
        is >> d;
    } while (!t.setDay(d));
    return is;
}
