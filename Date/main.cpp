#include <iostream>
#include "src/Date.h"

int main() {
    Date a;
    Date b(2022, 10, 2);
    Date c(a);

    a = b;

    c.setDay(15);
    c.setDay(-15);
    c.setDay(41);

    b.setMonth(7);
    b.setMonth(0);
    b.setMonth(60);

    a.setYear(2017);
    a.setYear(-1);

    printf("Date A: ");
    a.showDate();
    printf(" valid: %d \nDate B: ", a.valid());
    b.showDate();
    printf(" valid: %d \nDate C: ", b.valid());
    c.showDate();
    printf(" valid: %d\n", c.valid());

    printf("\na==b: %d\n", a==b);
    printf("b!=c: %d\n", b!=c);
    printf("c<a: %d\n", c<a);
    printf("b<a: %d\n", b<a);
    printf("a<=c: %d\n", a<=c);
    printf("b>=a: %d\n", b>=a);

    b+=5;
    c-=12;

    a = b+12;
    b = c+15;

    printf("Date A: ");
    a.showDate();
    printf(" valid: %d \nDate B: ", a.valid());
    b.showDate();
    printf(" valid: %d \nDate C: ", b.valid());
    c.showDate();
    printf(" valid: %d\n", c.valid());

    Date d(2022,6,12);
    Date e(2023,7,20);

    printf("\n%d\n", e-d);

    Date k, n;
    std::cin >> k;
    std::cin >> n;

    std::cout << std::endl << 12+n;

    std::cout << std::endl << ++k << std::endl << n++;
    std::cout << std::endl << n--;
    std::cout << std::endl << n;
}
