#ifndef MYMATH_MATH_H
#define MYMATH_MATH_H

#include <cmath>
#include <iostream>
using namespace std;

class MyMath
{
    static inline double m_pi = atan(1) * 4;
public:
    static double PI(){
        return m_pi;
    }
    static double add(double x, double y);
    static double sub(double x, double y);
    static double mult(double x, double y);
    static double div(double x, double y);
    static int random(int x, int y);
    static double abs(double x);
    static double round(double x);
    static double floor(double x);
    static double max(double x, double y);
    static double min(double x, double y);
    static double pow(double x, int y);
};

double MyMath::add(double x, double y) {
    return x+y;
}

double MyMath::sub(double x, double y) {
    return x-y;
}

double MyMath::mult(double x, double y) {
    return x*y;
}

double MyMath::div(double x, double y) {
    return x/y;
}

int MyMath::random(int x, int y) {
    srand(time(0));
    return rand()%(y-x+1) + x;
}

double MyMath::abs(double x) {
    return std::abs(x);
}

double MyMath::round(double x) {
    return std::round(x);
}

double MyMath::floor(double x) {
    return std::floor(x);
}

double MyMath::max(double x, double y) {
    return std::max(x,y);
}

double MyMath::min(double x, double y) {
    return std::min(x,y);
}

double MyMath::pow(double x, int y) {
    return std::pow(x,y);
}

#endif //MYMATH_MATH_H
