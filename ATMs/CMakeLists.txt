cmake_minimum_required(VERSION 3.23)
project(ItStep)

set(CMAKE_CXX_STANDARD 23)

add_executable(ItStep main.cpp src/Currency.h src/Atm.h)
