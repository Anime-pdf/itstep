#ifndef ITSTEP_ATM_H
#define ITSTEP_ATM_H

#include "Currency.h"

class Atm {
    char* m_BankName;
    long m_Number;
    int m_AmountOfCurrency;
    Currency* m_Currency;

    Atm(char* name = "Privat", long number = 1)
    {
        if(number < 0)
            return;
        m_Number = number;
        setBankName(name);
        m_Currency = nullptr;
        m_AmountOfCurrency = 0;
    }

    const char* getBankName() const {return m_BankName;}
    long getNumber() const {return m_Number;}

    void setBankName(char* name) {
        m_BankName = new char[strlen(name)+1];
        strcpy(m_BankName, name);
    }

    int isAvailableCurrency(int banknote,const char*code){
        for (int i = 0; i < m_AmountOfCurrency; ++i) {
            if (!strcmp(m_Currency[i].getCode(),code) && m_Currency[i].getBanknote() == banknote)
                return m_Currency[i].getAmount();
        }
        return 0;
    }

    void addCurrency(Currency obj){
        Currency* tmp = new Currency[m_AmountOfCurrency+1];
        for (int i = 0; i < m_AmountOfCurrency; ++i) {
            tmp[i] = m_Currency[i];
        }
        tmp[m_AmountOfCurrency] = obj;
        m_AmountOfCurrency++;
    }

    void sortByBanknote(){
        Currency tmp;
        for (int i = 0; i < m_AmountOfCurrency-1; ++i) {
            for (int j = 0; j < m_AmountOfCurrency-1; ++j) {
                if (m_Currency[j].getBanknote() < m_Currency[j+1].getBanknote())
                {
                    tmp = m_Currency[i];
                    m_Currency[j] = m_Currency[j+1];
                    m_Currency[j+1] = tmp;
                }
            }
        }
    }

    void delIfZero() {
        int *indxs;
        int indx;
        for (int i = 0; i < m_AmountOfCurrency; ++i) {
            if (m_Currency[i].isEmpty())
                indx++;
        }
        indxs = new int[indx];
        for (int i = 0, j = 0; i < m_AmountOfCurrency; ++i) {
            if (m_Currency[i].isEmpty()) {
                indxs[j] = i;
                j++;
            }
        }
        Currency *tmp = new Currency[m_AmountOfCurrency - indx];
        for (int o = 0, j = 0; o < indx; ++o) {
            for (int i = indxs[o]+1; i < m_AmountOfCurrency; ++i) {
                if (i == o)
                    break;
                else {
                    tmp[j] = m_Currency[i];
                    j++;
                }
            }
        }
        m_Currency = tmp;
        m_AmountOfCurrency-=indx;
    }

    void withdrawCash(const char* code, int cash)
    {
        for (int i = 0; i < m_AmountOfCurrency; ++i) {
            if (!strcmp(m_Currency[i].getCode(), code))
                if (!(cash % m_Currency[i].getBanknote())) {
                    int c = m_Currency[i].getAmount() - cash/m_Currency[i].getBanknote()*m_Currency[i].getBanknote();
                    if(c < 0)
                        return;
                    m_Currency[i].setAmount(c);
                }
        }
    }

    void showInfo() {
        printf("\nBank: %s\nATM #%ld\nCurrency: %s\nMoney left: %d\n",
               m_BankName,
               m_Number,
               m_Currency->getCode(),
               m_Currency->getAmount()*m_Currency->getBanknote());
    }

};

#endif //ITSTEP_ATM_H
