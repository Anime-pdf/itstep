#ifndef ITSTEP_CURRENCY_H
#define ITSTEP_CURRENCY_H


#include <cstring>
#include <cstdio>

class Currency {
    char* m_Code;
    int m_Banknote;
    int m_Amount;
public:
    Currency(int banknote = 1, int amount = 0, char* code = "UAH"){
        setCode(code);
        setBanknote(banknote);
        setAmount(amount);
    }
    Currency& operator=(const Currency& object)
    {
        m_Banknote = object.m_Banknote;
        m_Amount = object.m_Amount;
        m_Code = new char[strlen(object.m_Code) + 1];
        strcpy(m_Code, object.m_Code);
        return *this;
    }
    Currency(const Currency& object)
    {
        m_Banknote = object.m_Banknote;
        m_Amount = object.m_Amount;
        m_Code = new char[strlen(object.m_Code) + 1];
        strcpy(m_Code, object.m_Code);
    }
    ~Currency() {delete[] m_Code;}

    const char* getCode() const {return m_Code;}
    int getAmount() const {return m_Amount;}
    int getBanknote() const {return m_Banknote;}

    void setCode(char* code) {
        m_Code = new char[strlen(code)+1];
        strcpy(m_Code, code);
    }
    void setAmount(int amount) {if(amount<0)return;m_Amount = amount;}
    void setBanknote(int banknote) {if(banknote<1)return;m_Banknote = banknote;}

    bool isEmpty() {return !m_Amount;}

    void toBinFile(FILE* fp){
        int size = strlen(m_Code);
        fwrite(&size, sizeof (int), 1, fp);
        fwrite(m_Code, sizeof (char), size, fp);
        fwrite(&m_Banknote, sizeof (int), 1, fp);
        fwrite(&m_Amount, sizeof (int), 1, fp);
    }
    void fromBinFile(FILE* fp){
        int size;
        fread(&size, sizeof (int), 1, fp);
        m_Code = new char[size+1];
        fread(m_Code, sizeof (char), size, fp);
        m_Code[size] = '\0';
        fread(&m_Banknote, sizeof (int), 1, fp);
        fread(&m_Amount, sizeof (int), 1, fp);
    }
};


#endif //ITSTEP_CURRENCY_H
