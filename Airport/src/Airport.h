#ifndef AIRPORT_H
#define AIRPORT_H

#include <vector>
#include <algorithm>
#include "Airplane.h"

class Airport {
    std::vector<Airplane> _schedule;
public:
    bool FlightExists(int flightN)
    {
        return (std::find_if(_schedule.begin(), _schedule.end(), [flightN](const Airplane &p) {
            if (p.getFlightNumber() == flightN) return true; return false;
        }) != _schedule.end());
    }

    bool Add(int n, const std::string& direction, int passengers, int max_passengers)
    {
        return Add({n,direction,passengers,max_passengers});
    }

    bool Add(const Airplane& airplane)
    {
        if(FlightExists(airplane.getFlightNumber()))
            return false;
        _schedule.emplace_back(airplane);
        return true;
    }

    bool Remove(int n)
    {
        auto res = std::find_if(_schedule.begin(), _schedule.end(), [n](const Airplane& airplane){if(airplane.getFlightNumber() == n)return true;return false;});
        if (res != _schedule.end())
            _schedule.erase(res);
        else
            return false;
        return true;
    }

    bool Edit(int n, const std::string& NewDirection, int NewPassengers, int NewMaxPassengers)
    {
        if(!FlightExists(n))
            return false;
        std::replace_if(_schedule.begin(), _schedule.end(),[n](const Airplane& airplane){if(airplane.getFlightNumber() == n)return true;return false;}, Airplane(n, NewDirection, NewPassengers, NewMaxPassengers));
        return true;
    }

    Airplane Find(int n) {
        if(FlightExists(n))
            return *(std::find_if(_schedule.begin(), _schedule.end(), [n](const Airplane &p) {
                if (p.getFlightNumber() == n) return true; return false;
            }));
        return {0,"",0,0};
    }
};

#endif //AIRPORT_H
