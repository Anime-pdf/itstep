#ifndef AIRPORT_AIRPLANE_H
#define AIRPORT_AIRPLANE_H

#include <string>

class Airplane {
    int _n;
    std::string _direction;
    int _passengers;
    int _passengers_max;


    bool setMaxPassengers(int passengers)
    {
        if(passengers < 0)
            return false;
        _passengers_max = passengers;
        return true;
    }
public:
    Airplane(int n, const std::string& direction, int passengers, int max_passengers)
    {
        _n = n;
        _direction = direction;
        if(!setMaxPassengers(max_passengers))
            max_passengers = 200;
        if(!setPassengers(passengers))
            passengers = 0;
    }

    int getFlightNumber() const {return _n;}
    std::string getDirection() const {return _direction;}
    int getPassengers() const {return _passengers;}
    int getMaxPassengers() const {return _passengers_max;}

    bool setPassengers(int passengers)
    {
        if(passengers > _passengers_max || passengers < 0)
            return false;
        _passengers = passengers;
        return true;
    }

    bool operator==(const Airplane& airplane) const
    {
        if(airplane._n == _n)
            return true;
        return false;
    }
    Airplane& operator++()
    {
        setPassengers(_passengers+1);
        return *this;
    }
    Airplane& operator--()
    {
        setPassengers(_passengers-1);
        return *this;
    }
    bool operator>(const Airplane& airplane) const
    {
        if(airplane._passengers_max > _passengers_max)
            return false;
        return true;
    }
    bool operator<(const Airplane& airplane) const
    {
        if(airplane._passengers_max > _passengers_max)
            return true;
        return false;
    }
};

#endif //AIRPORT_AIRPLANE_H
