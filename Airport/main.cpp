#include <iostream>
#include <vector>
#include <src/Date.h>
#include <algorithm>

template<class T>
void print_vec(std::vector<T> v)
{
    for (const auto &item: v)
        std::cout << item << " ";
    std::cout << std::endl;
}

int main() {
    //2
    std::vector<Date> dates;
    dates.emplace_back();
    dates.emplace_back(1,5,10);
    dates.emplace_back(2012,6,15);
    dates.emplace_back(2036,1,20);
    dates.emplace_back(1999,12,1);
    dates.emplace_back(2023,3,3);
    print_vec(dates);
    std::cout << std::endl;

    {
        auto res = std::find(dates.begin(), dates.end(), Date(2012, 6, 15));
        if (res != dates.end())
            std::cout << "Found: " << *res << std::endl;
        else
            std::cout << "Not Found" << std::endl;
        std::cout << std::endl;
    }
    {
        auto res = std::min_element(dates.begin(), dates.end());
        if (res != dates.end()) {
            std::cout << "Found and erased: " << *res << std::endl;
            dates.erase(res);
        } else
            std::cout << "Min Element Not Found ???" << std::endl;
        print_vec(dates);
        std::cout << std::endl;
    }
    {
        auto res = std::find_if(dates.begin(), dates.end(), [](const Date& date){if(date.getMonth() == 1)return true;return false;});
        if (res != dates.end()) {
            std::cout << "Found and erased: " << *res << std::endl;
            dates.erase(res);
        }
        else
            std::cout << "Element with 1st month Not Found" << std::endl;
        print_vec(dates);
        std::cout << std::endl;
    }
    {
        std::replace_if(dates.begin(), dates.end(), [](const Date& date){ if (date < Date())return true;return false;}, Date());
        std::cout << "Past dates were replaced" << std::endl;
        print_vec(dates);
        std::cout << std::endl;
    }
    {
        std::sort(dates.begin(), dates.end());
        std::cout << "Sorted asc" << std::endl;
        print_vec(dates);
        std::sort(dates.rbegin(), dates.rend());
        std::cout << "Sorted desc" << std::endl;
        print_vec(dates);
        std::cout << std::endl;
    }

    //3

    return 0;
}
